-- phpMyAdmin SQL Dump
-- version 3.4.10
-- http://www.phpmyadmin.net
--
-- Host: dualtec-mysql-02.dualtec.com.br
-- Generation Time: Sep 17, 2015 at 03:47 PM
-- Server version: 5.5.19
-- PHP Version: 5.2.6-1+lenny16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `metabpo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientes_nome` varchar(255) NOT NULL,
  `clientes_ordem` int(10) NOT NULL,
  `clientes_imagem` varchar(255) NOT NULL,
  `clientes_desafio` text NOT NULL,
  `clientes_solucao` text NOT NULL,
  `clientes_resultados` text NOT NULL,
  `clientes_desde` int(4) NOT NULL,
  `clientes_setor` varchar(255) NOT NULL,
  `clientes_unidade` varchar(255) NOT NULL,
  `clientes_depoimento` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `clientes_nome`, `clientes_ordem`, `clientes_imagem`, `clientes_desafio`, `clientes_solucao`, `clientes_resultados`, `clientes_desde`, `clientes_setor`, `clientes_unidade`, `clientes_depoimento`) VALUES
(11, 'Bayer', 1, 'bayer-logo-grande.png', '<p>Em 2009 a Bayer Health Care reestruturou parte da sua planta fabril no bairro de Socorro – SP. Ao mesmo tempo, a Bayer Schering estava admitindo profissionais com a mesma qualificação. A Meta BPO realizou todo o processo de Recrutamento Interno.</p>', '<p>Disponibilizamos uma especialista em Recrutamento e Seleção <em>full time</em> para o projeto.</p>', '<p>Todas as posições foram fechadas com reaproveitamento interno.</p>', 1986, 'Indústria Farmacêutica', 'Meta BPO', ''),
(12, 'LIBBS', 2, 'libs-logo-grande.png', '<p>Processo de Recrutamento e Seleção de 105 posições: Auxiliares de Produção, Operadores de Máquinas, Analistas de Laboratório, Eletricistas, Instrumentistas e Mecânicos de Manutenção, para criação do terceiro turno da planta fabril em Embu das Artes – SP.</p>', '<p>Montamos uma célula dedicada com uma equipe de 4 analistas e 2 assistentes, trabalhando em tempo integral nestas posições.</p>', '<p>90% das vagas foram fechadas no prazo de 20 dias.</p>', 2008, 'Indústria Farmacêutica', 'Meta BPO', ''),
(13, 'Alcon', 3, 'alcon-logo-grande.png', '<p>Processo de Recrutamento e Seleção para demanda temporária na unidade fabril, para posições operacionais.</p>', '<p>Montamos uma equipe especializada composta por Analistas e Assistentes, trabalhando em tempo integral nestas posições.</p>', '<p>Vagas fechadas em 5 dias.</p>', 1984, 'Indústria Farmacêutica', 'Meta BPO', ''),
(14, 'CARREFOUR', 4, 'carrefour-logo-grande.png', '<p>Atender demanda estratégica de temporários no Estado de São Paulo.</p>', '<p>Recrutamento & Seleção de temporários e terceiros. Equipe alinhada com toda a operação do cliente. O atendimento inicia-se na seleção e vai até administração de todos os contratos e contratados.</p>', '<p>Em um ano de parceria, reduzimos os problemas de turn over.</p>', 1999, 'Comércio e varejo', 'Meta BPO', ''),
(15, 'BAXTER', 5, 'baxter-logo-grande.png', '<p>Processo de Recrutamento e Seleção de 70 posições de Auxiliares de Produção no período de 5 dias.</p>', '<p>Montamos uma célula dedicada com uma equipe de 2 analistas, 2 assistentes e 1 estagiária, trabalhando em tempo integral nestas posições.</p>', '<p>70 posições fechadas no prazo solicitado.</p>', 1984, 'Setor: Indústria Médico-Hospitalar\n', 'Meta BPO', ''),
(16, 'McDonald''s', 6, 'mcdonalds-logo-grande.png', '<p>Atender demanda estratégica de temporários na unidade corporativa.</p>', '<p>Processos de Recrutamento & Seleção para demanda temporária, direcionando uma equipe alinhada com toda operação do cliente para o atendimento. Inicia-se na seleção e vai até administração de todos os contratos e contratados.</p>', '<p>100% de assertividade nas contratações e solidificação da parceria para atender demandas efetivas e terceiras.</p>', 2011, 'Comércio e varejo', 'Meta BPO', ''),
(17, 'UNILEVER', 7, 'unilever-logo-grande.png', '<p>Contratações em nível nacional para posições de coordenadores, supervisores e gerentes, para todas as fábricas e corporativo da UNILEVER.</p>', '<p>Célula customizada com equipe treinada e alinhada às políticas do cliente.</p>', '<p>Contratação de mais de 25 posições em 3 meses de parceria no modelo proposto.</p>', 1998, 'Indústria de Alimentos, Bebidas e Higiene e Beleza.', 'Meta BPO', ''),
(18, 'Grupo Fleury', 8, 'fleury-logo-grande.png', '<p>Ser parceiro efetivo no projeto de expansão da rede A+, pertencente ao GRUPO FLEURY, possuir soluções em R&S para atendimento de todos os níveis de vagas.</p>', '<p>Célula dedicada em R&S e Administração de Contratos. Montamos uma unidade exclusiva de atendimento ao cliente, construída em parceria com o Grupo Fleury.</p>', '<p>Atendimento com resultados efetivos, com capacidade de fechamento de vagas em todos os níveis.</p>', 1998, 'Serviços – Medicina Diagnóstica.', 'Meta BPO', ''),
(19, 'NET', 10, 'net-logo-grande.png', '<p>Processos de Recrutamento e Seleção para atender de forma permanente 60 posições mensais.</p>', '<p>Criação de uma célula permanente, composta por 1 Analista Sênior, 1 Analista Pleno e 1 Assistente. Trabalhos realizados nas unidades da Meta BPO e NET.</p>', '<p>A Meta BPO se destacou pelo fechamento do maior número de posições entre todos os fornecedores.</p>', 2008, 'Serviços de Telecomunicações', 'Meta BPO', '');

-- --------------------------------------------------------

--
-- Table structure for table `destaques`
--

CREATE TABLE IF NOT EXISTS `destaques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cargo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `destaques`
--

INSERT INTO `destaques` (`id`, `empresa`, `cargo`, `link`) VALUES
(24, 'Empresa multinacional de grande porte, do segmento farmacêutico', 'Eletricista Manutenção', 'http://www.vagas.com.br/v1172892'),
(18, 'Empresa nacional, de grande porte, do segmento medicina diagnostica', 'Analista de Indicadores', 'http://www.vagas.com.br/v1168338'),
(22, 'Empresa multinacional de médio porte, do segmento de automação', 'Técnico Volante', 'http://www.vagas.com.br/v1162792'),
(23, 'Empresa multinacional de grande porte, do segmento financeiro', 'Agente de Cadastro Positivo', 'http://www.vagas.com.br/v1165490');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_20_125430_create_usuarios_table', 1),
('2014_05_20_153903_create_banners_table', 1),
('2014_05_20_153922_create_quem_somos_table', 1),
('2014_05_20_153956_create_diferenciais_table', 1),
('2014_05_20_154142_create_oportunidades_table', 1),
('2014_05_20_154158_create_cases_table', 1),
('2014_05_20_154226_create_contato_table', 1),
('2014_05_22_144038_create_candidatos_table', 1),
('2014_05_22_180816_create_depoimentos_table', 1),
('2014_05_23_132403_create_solucoes_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletters_nome` varchar(255) NOT NULL,
  `newsletters_email` varchar(255) NOT NULL,
  `newsletters_vagas` tinyint(1) NOT NULL,
  `newsletters_novidades` tinyint(1) NOT NULL,
  `newsletters_eventos` tinyint(1) NOT NULL,
  `newsletters_resultados` tinyint(1) NOT NULL,
  `newsletters_noticias` tinyint(1) NOT NULL,
  `newsletters_data_cadastro` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `newsletters_nome`, `newsletters_email`, `newsletters_vagas`, `newsletters_novidades`, `newsletters_eventos`, `newsletters_resultados`, `newsletters_noticias`, `newsletters_data_cadastro`) VALUES
(1, 'Nilton', 'niltondefreitas@gmail.com', 1, 1, 1, 1, 1, 1353429304);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `noticia_titulo` varchar(255) NOT NULL,
  `noticia_conteudo` text NOT NULL,
  `noticia_imagem` varchar(255) NOT NULL,
  `noticia_data_publicacao` int(10) NOT NULL,
  `noticia_resumo` varchar(400) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `noticia_titulo`, `noticia_conteudo`, `noticia_imagem`, `noticia_data_publicacao`, `noticia_resumo`) VALUES
(8, 'Fofocas, Fuxicos e Mentiras no Ambiente de Trabalho! ', '<p>Num mundo aonde a informa&ccedil;&atilde;o chega em tempo real, a mentira, a fofoca e os "fofoqueiros de plant&atilde;o" foram beneficiados com a melhora da tecnologia.</p>\n<p>Mas qual &eacute; a diferen&ccedil;a entre mentira e fofoca?</p>\n<p>A mentira &eacute; uma afirma&ccedil;&atilde;o cujos fatos n&atilde;o correspondem &agrave; verdade. Geralmente as pessoas mentem com o objetivo de evitar uma poss&iacute;vel puni&ccedil;&atilde;o ou de encobrir uma situa&ccedil;&atilde;o rid&iacute;cula. Pode ser tamb&eacute;m uma estrat&eacute;gia para n&atilde;o comprometer outras pessoas injustamente. Mentiras ocorrem com bastante frequ&ecirc;ncia no ambiente de trabalho.</p>\n<p>A quest&atilde;o &eacute; que a v&iacute;tima de uma mentira sempre est&aacute; em desvantagem porque n&atilde;o sabe a verdade e n&atilde;o tem a informa&ccedil;&atilde;o correta para tomar uma decis&atilde;o acertada at&eacute; que a verdade se imponha. A v&iacute;tima de uma mentira acredita naquilo que sup&otilde;e verdadeiro quando n&atilde;o o &eacute;. Agindo sob influ&ecirc;ncia de algo que n&atilde;o &eacute; verdade, d&aacute; para imaginar as consequ&ecirc;ncias. Inclusive, h&aacute; v&aacute;rios exemplos de empresas que sucumbiram devido a mentiras e "maquiagens" em sua contabilidade.</p>\n<p>Enquanto a mentira &eacute; uma informa&ccedil;&atilde;o falsa uma fofoca &eacute; um argumento falso ou um argumento mal direcionado ou conduzido de forma planejada a atingir maldosamente uma pessoa ou grupo de pessoas.</p>\n<p>A fofoca &eacute; algo inventado e parte de pessoas com o seguinte perfil: baixa auto-estima, desconfiadas, gostam de jogar uma pessoa contra a outra, tendo necessidade de dividir e conquistar grupos, com sentimento permanente de raiva, buscando a autoafirma&ccedil;&atilde;o.</p>\n<p>Quem faz fofoca est&aacute; na realidade fazendo "fuxico", intriga, "mexerico". E h&aacute; diversos tipos de fofoqueiros, dentre os quais: os que s&atilde;o contra tudo e todos; os que vivem "ca&ccedil;ando" a falha dos outros, por menores que elas sejam; aqueles que apenas subtraem e dividem ao inv&eacute;s de somar e multiplicar; os que interpretam tudo na "contram&atilde;o"; aqueles que se fazem de "coitadinhos"; os maquiav&eacute;licos...e muitos outros tipos.</p>\n<p>O grande problema &eacute; que esses "pseudoprofissionais" s&atilde;o os que geram conflitos no ambiente de trabalho, fazendo com que o clima organizacional se deteriore rapidamente e de forma constante e consistente, tornando a comunica&ccedil;&atilde;o hostil e com o crescimento da desconfian&ccedil;a nas rela&ccedil;&otilde;es interpessoais.</p>\n<p>Publicado em 05/10/2012 &agrave;s 20:16:22<br />Escrito por ALBERTO RUGGIERO<br />Fonte: <a href="http://www.abtd.com.br" target="_blank">www.abtd.com.br</a></p>', '', 1354845600, 'Enquanto a mentira é uma informação falsa uma fofoca é um argumento falso ou um argumento mal direcionado ou conduzido de forma planejada a atingir maldosamente uma pessoa ou grupo de pessoas. '),
(9, 'Pesquisa aponta os benefícios mais oferecidos às mulheres nas empresas', '<p><br />Com o objetivo de identificar o movimento das empresas para se adaptar &agrave; nova realidade do mercado de trabalho, no qual as mulheres j&aacute; s&atilde;o mais de 53%, segundo levantamento do IBGE (Instituto Brasileiro de Geografia e Estat&iacute;stica), a consultoria Towers Watson realizou uma pesquisa, em agosto, com 120 empresas, para saber os benef&iacute;cios que est&atilde;o sendo oferecidos especificamente para este p&uacute;blico interno.</p>\n<p><br />O levantamento detectou, entre os resultados, que 33% oferecem hor&aacute;rio flex&iacute;vel ap&oacute;s o per&iacute;odo da licen&ccedil;a-maternidade, 16% possibilitam o home office durante e ap&oacute;s o per&iacute;odo gestacional, 18% isentam a coparticipa&ccedil;&atilde;o da funcion&aacute;ria nos planos de sa&uacute;de durante o per&iacute;odo pr&eacute;-natal e 60% oferecem conv&ecirc;nio ou algum tipo de subs&iacute;dio para planos de condicionamento f&iacute;sico.</p>\n<p><br />Apesar de facultativa, a licen&ccedil;a-maternidade de seis meses foi aderida por 44% das companhias. Postos de atendimento ou servi&ccedil;os de apoio &agrave; gestante, com acompanhamento ginecol&oacute;gico e psicol&oacute;gico, s&atilde;o oferecidos por 22% e dieta balanceada durante o per&iacute;odo gestacional, por 18%.</p>\n<p><br />No tocante aos programas de preven&ccedil;&atilde;o de doen&ccedil;as espec&iacute;ficas do universo feminino, 37,6% oferecem algum tipo de palestra ou acompanhamento. J&aacute; o aux&iacute;lio creche foi incorporado por 84%. E, apesar de as empresas ainda estarem se estruturando para receber os filhos das funcion&aacute;rias, 32% elevam o limite de idade de elegibilidade do benef&iacute;cio &agrave;s crian&ccedil;as acima de tr&ecirc;s anos, mas apenas 8% dos empregadores oferecem ber&ccedil;&aacute;rio ou lact&aacute;rio em suas instala&ccedil;&otilde;es.</p>\n<p>Fonte: <a href="http://www.abrhnacional.org.br/noticias/1154--pesquisa-aponta-os-beneficios-mais-oferecidos-as-mulheres-nas-empresas.html" target="_blank">www.abrhnacional.com.br</a></p>', '', 1354500000, 'Pesquisa aponta os benefícios mais oferecidos às mulheres nas empresas'),
(10, 'Grupo Meta RH recebe cinco distinções no Prêmio Melhores Fornecedores para RH', '<p>&nbsp;</p>\n<p>Al&eacute;m de figurar entre os 10, 100 e 300 Melhores Fornecedores para RH, consultoria tamb&eacute;m recebeu dois pr&ecirc;mios na categoria Talentos</p>\n<p>O Grupo Meta RH recebeu no dia 5 de fevereiro cinco distin&ccedil;&otilde;es no Pr&ecirc;mio 100 Melhores Fornecedores para RH, organizado pela Gest&atilde;o &amp; RH Editora. Al&eacute;m de figurar entre os 10, 100 e 300 Melhores Fornecedores para RH, a consultoria tamb&eacute;m recebeu os pr&ecirc;mios de Melhor Avaliada em Recrutamento e Sele&ccedil;&atilde;o e Trabalho Efetivo e Tempor&aacute;rio na categoria Talentos. Abigair Ribeiro Costa Leite, diretora administrativa e financeira, representou o Grupo Meta RH na cerim&ocirc;nia de premia&ccedil;&atilde;o. Organizado pela revista Gest&atilde;o &amp; RH, o evento tem por objetivo reconhecer o trabalho, atendimento e qualidade das empresas que atendem ao setor de Recursos Humanos.</p>\n<p>A premia&ccedil;&atilde;o &eacute; realizada em duas fases. Na primeira a vota&ccedil;&atilde;o envolve os profissionais que atuam na &aacute;rea de Gest&atilde;o de Pessoas, que escolhem at&eacute; dez empresas dos 40 segmentos selecionados no estudo &ldquo;300 Melhores Fornecedores para RH&rdquo;. As empresas mais votadas s&atilde;o classificadas para a segunda fase, na qual s&atilde;o votadas pelos seus pr&oacute;prios clientes. A cerim&ocirc;nia de premia&ccedil;&atilde;o tamb&eacute;m contou com a palestra da escritora Leila Navarro.</p>', '', 1360893600, 'Além de figurar entre os 10, 100 e 300 Melhores Fornecedores para RH, consultoria também recebeu dois prêmios na categoria Talentos'),
(11, 'Novo modelo de rescisão já está em vigor', '<p>Desde o dia&nbsp;1&ordm; de fevereiro, todas as rescis&otilde;es de contrato de trabalho devem utilizar o novo modelo do Termo de Rescis&atilde;o do Contrato de Trabalho (TRCT),&nbsp; institu&iacute;do pelo Minist&eacute;rio do Trabalho e Emprego (MTE). Junto com o novo termo devem ser utilizados os formul&aacute;rios do&nbsp;Termo de Quita&ccedil;&atilde;o, para as rescis&otilde;es de contrato de trabalho com menos de um ano de servi&ccedil;o, e o Termo de Homologa&ccedil;&atilde;o,&nbsp;para as rescis&otilde;es com mais de um ano de servi&ccedil;o.</p>\n<p class="Normal2">Entre as principais mudan&ccedil;as institu&iacute;das est&aacute; a necessidade de informar separadamente f&eacute;rias e 13&ordm; sal&aacute;rio vencidos e n&atilde;o quitados. Tamb&eacute;m constam no novo modelo campos espec&iacute;ficos para detalhamento de horas-extras devidas, cr&eacute;ditos ao trabalhador, al&eacute;m de descontos e dedu&ccedil;&otilde;es. O novo formul&aacute;rio deve ser impresso em duas vias, sendo uma para o empregador e outra para o empregado.</p>\n<p class="Normal2">Este deve ser acompanhado do Termo de Homologa&ccedil;&atilde;o ou de Quita&ccedil;&atilde;o, impresso em quatro vias, uma para o empregador e tr&ecirc;s para o empregado, destinadas ao saque do Fundo de Garantia por Temo de Servi&ccedil;o (FGTS) e solicita&ccedil;&atilde;o do seguro-desemprego. &ldquo;O novo modelo tem por objetivo dar mais seguran&ccedil;a ao trabalhador e ao empregador na hora da rescis&atilde;o, uma vez que todas as parcelas devidas e pagas devem ser detalhadas, ao contr&aacute;rio do que ocorre com o termo atual. E o trabalhador deve ficar atento, pois rescis&otilde;es feitas em outros modelos n&atilde;o ser&atilde;o mais aceitas pela Caixa Econ&ocirc;mica Federal para libera&ccedil;&atilde;o de Seguro-Desemprego e do FGTS&rdquo;, explica o especialista em Gest&atilde;o Tribut&aacute;ria e Fiscal da Alterdata, Mauro Moraes. (META BPO)</p>', '', 1363748400, 'Formulário anterior não será mais aceito para liberação de benefícios'),
(12, 'Restaurantes McDonald''s premiam funcionários com bolsas de estudo', '<p>&nbsp;</p>\n<p class="Normal2">Como recompensa pela melhor performance em suas atividades, cinco funcion&aacute;rios de restaurantes no Rio de Janeiro e Minas Gerais foram premiados com bolsas de estudos para Ensino Superior, cursos de idiomas ou inform&aacute;tica. Ao todo, foram contemplados 36 funcion&aacute;rios em todo o pa&iacute;s.</p>\n<p class="Normal2">Os vencedores foram eleitos por meio do All Star, a principal a&ccedil;&atilde;o dentro do programa de incentivos da empresa. Funcion&aacute;rios de todo o pa&iacute;s s&atilde;o avaliados em suas diferentes atividades, testando seus conhecimentos t&eacute;cnicos sobre qualidade, servi&ccedil;o e limpeza.</p>\n<p class="Normal2">Para concorrer ao pr&ecirc;mio, os funcion&aacute;rios passaram por fases de sele&ccedil;&atilde;o das melhores performances em tr&ecirc;s esta&ccedil;&otilde;es em &aacute;reas diferentes dos restaurantes. Eles concorreram localmente, com os colegas de restaurantes pr&oacute;ximos, depois regionalmente, com funcion&aacute;rios de unidades de outras cidades. E, por &uacute;ltimo, passaram pela fase que envolve funcion&aacute;rios de todo o pa&iacute;s. &ldquo;O All Star envolve toda a empresa. E os funcion&aacute;rios se sentem engajados e motivados, porque s&atilde;o reconhecidos por todos. Inclusive, na &uacute;ltima fase, s&atilde;o avaliados pelos principais executivos da companhia&rdquo;, afirma Ana Apolaro, diretora de Recursos Humanos da Arcos Dourados, organiza&ccedil;&atilde;o que opera a marca McDonald&rsquo;s no Brasil e &eacute; cliente da Meta BPO, empresa do Grupo Meta RH.</p>\n<p class="Normal2">Um dos vencedores, Rafael Pinheiro, come&ccedil;ou como atendente de restaurante e recentemente foi promovido a treinador. Para ele, ganhar um concurso como esse &eacute; muito importante para carreira profissional. &ldquo;Vale muito a pena se esfor&ccedil;ar, se dedicar e participar. A sensa&ccedil;&atilde;o de felicidade por ter feito um bom trabalho &eacute; maravilhosa&rdquo;, afirma Rafael.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2"><strong>All Star</strong></p>\n<p class="Normal2">Criado h&aacute; mais 20 anos, o programa de incentivo All Star &eacute; aprimorado a cada ano. No in&iacute;cio, os vencedores recebiam trof&eacute;us e outros pr&ecirc;mios que j&aacute; eram um est&iacute;mulo &agrave; participa&ccedil;&atilde;o na disputa. H&aacute; sete anos, al&eacute;m destes pr&ecirc;mios, a empresa decidiu conceder bolsas de estudo a todos os campe&otilde;es.</p>', '', 1363748400, 'Vencedores da competição All Star foram eleitos os melhores profissionais no programa de incentivo da empresa'),
(13, 'O trânsito e as novas relações de trabalho', '<p>&nbsp;</p>\n<p>&ldquo;N&atilde;o importa onde voc&ecirc; trabalha, h&aacute; mais de uma forma de criar um projeto, em que o entusiasmo &eacute; palp&aacute;vel, onde algo que possa fazer a diferen&ccedil;a esteja ao virar a esquina.&rdquo;</p>\n<p>(Marcelo Costa)</p>\n<p class="Normal2"><em>&nbsp;</em></p>\n<p class="Normal2">Mobilidade. Este &eacute; um dos maiores desafios de um mundo com sete bilh&otilde;es de pessoas. E n&atilde;o apenas para as grandes cidades.</p>\n<p class="Normal2">N&atilde;o &eacute; o direito, mas a capacidade de ir e vir que est&aacute; comprometida. Dia destes, devido a uma chuva intensa e prolongada, levei quase duas horas para percorrer seis quil&ocirc;metros.</p>\n<p class="Normal2">A quest&atilde;o ambiental est&aacute; em voga. Por&eacute;m, poucos percebem que um carro trafegando a uma velocidade m&eacute;dia de pedestre consome mais pneus e pastilhas de freio, aumenta sua deprecia&ccedil;&atilde;o e eleva o consumo de combust&iacute;vel &agrave;s alturas, despejando mais gases de efeito estufa e poluindo ainda mais o ar.</p>\n<p class="Normal2">A falta de transporte coletivo em quantidade e de qualidade impede a integra&ccedil;&atilde;o do autom&oacute;vel com as redes de &ocirc;nibus, trem e metr&ocirc;. E mesmo as esta&ccedil;&otilde;es inauguradas recentemente n&atilde;o disp&otilde;em de edif&iacute;cio-garagem para estimular o motorista a rodar menos com seu carro.</p>\n<p class="Normal2">A mobilidade n&atilde;o &eacute; apenas um problema de infraestrutura vi&aacute;ria, mas de sa&uacute;de p&uacute;blica. Afora a perda material, h&aacute; efeitos intang&iacute;veis e de dif&iacute;cil mensura&ccedil;&atilde;o. Primeiro, o tempo das pessoas, que deixam de cumprir compromissos e realizar tarefas diversas porque est&atilde;o presas no tr&acirc;nsito. Reuni&otilde;es canceladas, clientes e pacientes n&atilde;o atendidos, produ&ccedil;&atilde;o que deixa de ser gerada.</p>\n<p class="Normal2">Segundo, o estresse. A paci&ecirc;ncia, a toler&acirc;ncia e o equil&iacute;brio emocional s&atilde;o colocados &agrave; prova ao extremo, trazendo &agrave; tona o que h&aacute; de pior nas pessoas. Motoristas que n&atilde;o d&atilde;o passagem a pedestres e a outros ve&iacute;culos, motociclistas que trafegam irresponsavelmente em alta velocidade pelo corredor formado entre as faixas de rolagem, gente que ocupa assentos preferenciais no transporte coletivo e n&atilde;o os cedem a quem de direito.</p>\n<p class="Normal2">Ali&aacute;s, s&atilde;o os mais pobres os que mais sofrem. O desenvolvimento econ&ocirc;mico empurrou os trabalhadores para a periferia das cidades, exigindo-lhes despertar no final da madrugada para se dirigirem ao trabalho, desperdi&ccedil;ando at&eacute; seis horas di&aacute;rias para ir e voltar.</p>\n<p class="Normal2">Nossa sociedade est&aacute; privando as pessoas do conv&iacute;vio familiar e minando a sa&uacute;de de seus trabalhadores. Aumentam os acidentes de trabalho, em decorr&ecirc;ncia do cansa&ccedil;o e da desaten&ccedil;&atilde;o durante a atividade laboral, bem como os acidentes de trajeto. Crescem o absente&iacute;smo, o presente&iacute;smo, as doen&ccedil;as profissionais e psicossom&aacute;ticas.</p>\n<p class="Normal2">Enquanto os governos n&atilde;o fazem sua parte, mitigando os j&aacute; mencionados problemas de infraestrutura, &eacute; premente repensar as rela&ccedil;&otilde;es de trabalho, incentivando o trabalho &agrave; dist&acirc;ncia, mediante uma gest&atilde;o por confian&ccedil;a capaz de prescindir da presen&ccedil;a f&iacute;sica dos colaboradores na sede da empresa. Precisamos avan&ccedil;ar na qualidade da conex&atilde;o de banda larga para estimular as videoconfer&ecirc;ncias. S&atilde;o muitas as quest&otilde;es que podem e devem ser resolvidas online ou por telefone. O trabalho SOHO (<em>small office, home office</em>) &eacute; uma necessidade hoje e n&atilde;o do futuro.</p>\n<p class="Normal2">&Eacute; evidente que temos uma legi&atilde;o de trabalhadores operacionais que jamais poder&atilde;o ser inclu&iacute;dos neste sistema. Ainda nos anos de 1990 eu pretendia implantar uma jornada flex&iacute;vel, mas era impratic&aacute;vel dentro de uma atividade industrial na qual o processo produtivo &eacute; sequencial. Se o respons&aacute;vel pelo corte do a&ccedil;o decidisse chegar mais tarde, os soldadores n&atilde;o teriam mat&eacute;ria-prima para trabalhar, comprometendo todo o sistema.</p>\n<p class="Normal2">Por fim, cabe tamb&eacute;m um alerta aos profissionais. Conhe&ccedil;o uma empresa que oferece caf&eacute; e p&atilde;o com manteiga &agrave;s oito horas da manh&atilde; aos seus vendedores apenas para exigir-lhes a presen&ccedil;a no escrit&oacute;rio antes de sa&iacute;rem &agrave; rua para as visitas agendadas, impedindo que alguns resolvam simplesmente dormir at&eacute; depois das dez, expondo mais do que sua falta de profissionalismo, a aus&ecirc;ncia de comprometimento.</p>\n<p class="Normal2"><em>&nbsp;</em></p>\n<p class="Normal2"><em>* <strong>Tom Coelho</strong> &eacute; educador, conferencista e escritor com artigos publicados em 17 pa&iacute;ses. &Eacute; autor de &ldquo;Somos Maus Amantes &ndash; Reflex&otilde;es sobre carreira, lideran&ccedil;a e comportamento&rdquo; (Flor de Liz, 2011), &ldquo;Sete Vidas &ndash; Li&ccedil;&otilde;es para construir seu equil&iacute;brio pessoal e profissional&rdquo; (Saraiva, 2008) e coautor de outras cinco obras. Contatos atrav&eacute;s do e-mail </em><a href="mailto:tomcoelho@tomcoelho.com.br">tomcoelho@tomcoelho.com.br</a><em>. Visite: </em><a href="http://www.tomcoelho.com.br/">www.tomcoelho.com.br</a><em> e </em><a href="http://www.setevidas.com.br">www.setevidas.com.br</a><em>.</em></p>', '', 1364266800, 'Mobilidade. Este é um dos maiores desafios de um mundo com sete bilhões de pessoas. E não apenas para as grandes cidades.'),
(14, 'Estabilidade da gestante nos contratos de experiência e temporários', '<p>&nbsp;</p>\n<p class="Normal2">Em setembro de 2012, o Tribunal Superior do Trabalho reconheceu a estabilidade provis&oacute;ria de gestante, mesmo quando o contrato de trabalho for tempor&aacute;rio ou a titulo de experi&ecirc;ncia. Um dos fundamentos que orientou o entendimento do Tribunal Superior do Trabalho foi o de que o alvo da prote&ccedil;&atilde;o conferida pela Constitui&ccedil;&atilde;o da Rep&uacute;blica &eacute; tamb&eacute;m o nascituro. Nesse sentido, foi aprovada a seguinte reda&ccedil;&atilde;o para o inciso III da s&uacute;mula 244: &ldquo;A empregada gestante tem direito &agrave; estabilidade provis&oacute;ria prevista no art. 10, inciso II, al&iacute;nea b, do ADCT, mesmo na hip&oacute;tese de admiss&atilde;o mediante contrato por tempo determinado&rdquo;.</p>\n<p class="Normal2">Note-se que o desconhecimento do estado de gravidez, pelo empregador, n&atilde;o afasta o direito ao pagamento da indeniza&ccedil;&atilde;o decorrente da estabilidade. A garantia de emprego &agrave; gestante s&oacute; autoriza a reintegra&ccedil;&atilde;o se esta se der durante o per&iacute;odo de estabilidade, caso contr&aacute;rio, o empregador dever&aacute; indenizar todo o per&iacute;odo estabilit&aacute;rio.</p>\n<p class="Normal2">&nbsp;</p>\n<h1>E se a gravidez for constatada durante o aviso pr&eacute;vio?</h1>\n<p class="Normal2">O Tribunal Superior do Trabalho reconheceu, em decis&atilde;o hist&oacute;rica, o direito &agrave; estabilidade no emprego em raz&atilde;o da gravidez, ainda que a concep&ccedil;&atilde;o tenha ocorrido no curso do aviso pr&eacute;vio indenizado.</p>\n<p class="Normal2">No entanto, na realidade, tal decis&atilde;o pode n&atilde;o ser favor&aacute;vel &agrave;s trabalhadoras em geral: afinal, s&atilde;o estas decis&otilde;es judiciais que acabam pesando quando o empregador se depara com a decis&atilde;o entre contratar um candidato ou uma candidata &agrave; vaga de emprego. Isto &eacute; especialmente delicado para os empres&aacute;rios da &aacute;rea varejista, que dependem de trabalho tempor&aacute;rio nas &eacute;pocas de maior movimento (festas de final de ano, dia das m&atilde;es, etc.).</p>\n<p class="Normal2"><strong><em>&nbsp;</em></strong></p>\n<p class="Normal2"><strong><em>*D&eacute;bora Fernanda Faria &eacute; advogada trabalhista do escrit&oacute;rio Cerveira Advogados Associados &ndash; </em></strong><a href="mailto:debora@cerveiraadvogados.com.br" target="_blank"><strong><em>debora@cerveiraadvogados.com.br</em></strong></a></p>', '', 1364266800, 'o Tribunal Superior do Trabalho reconheceu a estabilidade provisória de gestante, mesmo quando o contrato de trabalho for temporário ou a titulo de experiência'),
(15, 'Você sabe como solicitar um aumento salarial?', '<p>&nbsp;</p>\n<p class="Crdito">&nbsp;</p>\n<p class="Normal2">Quem n&atilde;o quer um aumento de sal&aacute;rio? S&oacute; que antes de chegar diante do chefe pedindo pelo aumento, vale a pena uma dica: Cuidado, porque ele pode te perguntar &ldquo;o que voc&ecirc; tem feito para merecer um aumento?&rdquo;</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">- Estou me esfor&ccedil;ando;</p>\n<p class="Normal2">- Cumpro com o meu hor&aacute;rio;</p>\n<p class="Normal2">- Sou o primeiro a chegar e &uacute;ltimo a sair;</p>\n<p class="Normal2">- Veste a camisa da empresa;</p>\n<p class="Normal2">- &Eacute; um funcion&aacute;rio antigo e nunca pediu um aumento antes.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Em alguns casos essas respostas podem at&eacute; servir, mas na maioria das vezes os chefes dir&atilde;o que isso n&atilde;o &eacute; nada mais do que sua obriga&ccedil;&atilde;o e que voc&ecirc; j&aacute; ganha justamente para se esfor&ccedil;ar, fazer um trabalho bem feito e para ser pontual. E quanto a ser antigo na empresa&hellip; bem, talvez voc&ecirc; tenha passado despercebido esse tempo todo. Por que ser&aacute;?</p>\n<p class="Normal2">N&atilde;o esque&ccedil;a que a empresa &eacute; uma organiza&ccedil;&atilde;o com fins lucrativos e que todo investimento precisa ter um retorno que valha a pena o desembolso de determinada quantia. Mas, ao contr&aacute;rio de outros investimentos, quando se fala em sal&aacute;rio a coisa se inverte: o retorno deve vir antes do investimento.</p>\n<p class="Normal2">&Eacute; isso mesmo. Voc&ecirc; precisa primeiro mostrar resultados para depois solicitar o investimento (aumento de sal&aacute;rio). Se voc&ecirc; continua fazendo o que sempre fez da maneira que sempre fez e conseguindo os resultados que sempre conseguiu, pense comigo: por que seu chefe lhe daria um aumento? Se voc&ecirc; quer o aumento deve &ldquo;mostrar servi&ccedil;o&rdquo;, que merece ter um aumento de sal&aacute;rio.</p>\n<p class="Normal2">A dica &eacute;: antes de pedir um aumento, &eacute; melhor realizar uma autocr&iacute;tica que agregue os seguintes apontamentos:</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">- A empresa passa por um momento est&aacute;vel que permite aumentar meu sal&aacute;rio?</p>\n<p class="Normal2">- O meu sal&aacute;rio est&aacute; compat&iacute;vel com o mercado?</p>\n<p class="Normal2">- Tenho atendido as expectativas do meu gestor e da empresa?</p>\n<p class="Normal2">- Tenho lido material espec&iacute;fico referente ao meu trabalho?</p>\n<p class="Normal2">- Tenho realizado cursos de atualiza&ccedil;&atilde;o, p&oacute;s-gradua&ccedil;&atilde;o ou MBA?</p>\n<p class="Normal2">- Tenho sugerido sugest&otilde;es para melhorar a maneira de executar o trabalho?</p>\n<p class="Normal2">- Se eu fosse meu gestor daria um aumento para mim?</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Procure demonstrar para seu gestor que &eacute; mais vantajoso investir em voc&ecirc; dando exemplos de resultados alcan&ccedil;ados e onde seu desempenho favoreceu a companhia.</p>\n<p class="Normal2">Ent&atilde;o, neste momento, voc&ecirc; pode estar pensando: mas e se eu mostro excelentes resultados, trago lucro para a empresa, me esfor&ccedil;o, estou sempre me atualizando, dou sugest&otilde;es de melhoria e, mesmo assim, n&atilde;o consigo o aumento? O que fa&ccedil;o?</p>\n<p class="Normal2">Ent&atilde;o talvez seja o momento de voc&ecirc; procurar outro lugar para trabalhar, um lugar em que seu conhecimento, capacidade de trabalho e preocupa&ccedil;&atilde;o com resultados sejam devidamente reconhecidos.</p>\n<p class="Normal2">Ou ent&atilde;o fique quieto no seu canto!</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Luiz Eduardo Gasparetto &eacute; professor e coordenador dos cursos de MBA e Gest&atilde;o da Universidade Gama Filho. &Eacute; formado em Propaganda e Marketing pela Escola Superior de Propaganda de SP e em Direito pela PUC &ndash; Pontif&iacute;cia Universidade Cat&oacute;lica de S&atilde;o Paulo, e&nbsp;tem diversos cursos&nbsp;de especializa&ccedil;&atilde;o em Recursos Humanos e Administra&ccedil;&atilde;o. Foi coordenador de cursos da Associa&ccedil;&atilde;o Brasileira da Ind&uacute;stria Gr&aacute;fica, respons&aacute;vel pela cria&ccedil;&atilde;o e implanta&ccedil;&atilde;o da Escola de Aprendizes Gr&aacute;ficos e da Escola de Jornaleiros da Editora Abril e pela implanta&ccedil;&atilde;o do Projeto FORMATUR de prepara&ccedil;&atilde;o de m&atilde;o de obra hoteleira em Recife.</p>', '', 1364266800, 'Veja as dicas para saber está na hora de pedir um aumento'),
(16, 'Grupo Meta RH conquista Prêmio Fornecedores de Confiança 2013', '<p>&nbsp;</p>\n<p class="Normal2">O Grupo Meta RH, consultoria de solu&ccedil;&otilde;es em Recrutamento, Sele&ccedil;&atilde;o, Administra&ccedil;&atilde;o de Pessoal Terceirizado e Desenvolvimento Organizacional, foi premiado como um dos destaques do Pr&ecirc;mio Fornecedores de Confian&ccedil;a 2013, realizado pela Editora Segmento. A cerim&ocirc;nia de premia&ccedil;&atilde;o foi realizada dia 26 de mar&ccedil;o, no La Luna Club, em S&atilde;o Paulo, SP.</p>\n<p class="Normal2">O pr&ecirc;mio, direcionado a empresas que fornecem solu&ccedil;&otilde;es e prestam servi&ccedil;os para a &aacute;rea de Recursos Humanos, tem como objetivo reconhecer e destacar as institui&ccedil;&otilde;es que possuem o mais alto grau de confiabilidade. &ldquo;Estamos muito contentes com esse reconhecimento. Ser escolhida pela primeira vez como uma empresa destaque indica que as solu&ccedil;&otilde;es do Grupo Meta RH para a &aacute;rea de Recursos Humanos v&atilde;o ao encontro das necessidades de nossos clientes&rdquo;, afirma Gutemberg Leite, diretor comercial.</p>\n<p class="Normal2">O estudo para o Pr&ecirc;mio Fornecedores de Confian&ccedil;a 2013 foi realizado entre novembro de 2012 e fevereiro de 2013. Para participar, cada companhia inscrita indicou 40 clientes que responderam a uma pesquisa de satisfa&ccedil;&atilde;o. Foram considerados clientes v&aacute;lidos aqueles atendidos por um per&iacute;odo superior a seis meses.</p>\n<p class="Normal2">Em sua sexta edi&ccedil;&atilde;o, o evento premiou 50 empresas. Para chegar ao resultado, foram consultadas cerca de 4.800 empresas clientes de fornecedores de produtos e servi&ccedil;os para a &aacute;rea de RH.</p>', '', 1364439600, 'Estudo reconhece e destaca instituições com o mais alto grau de confiabilidade no segmento de Recursos Humanos');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_banners`
--

CREATE TABLE IF NOT EXISTS `novosite_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `novosite_banners`
--

INSERT INTO `novosite_banners` (`id`, `link`, `imagem`, `texto_pt`, `texto_en`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'empresa/diferenciais', '201505181329102.jpg', 'Encontramos as<br>melhores soluções<br>para o mercado<br>financeiro', 'We find the best solutions<br>for the financial market', 1, '2015-05-08 17:30:54', '2015-06-10 22:57:59'),
(2, 'empresa/quemsomos', '201505181329041.jpg', 'Há mais de 30 anos<br>atendendo as maiores<br>empresas do segmento<br>farmacêutico', 'Over 30 years<br>serving the largest companies<br>in the pharmaceutical segment', 0, '2015-05-08 17:31:05', '2015-06-10 22:56:13'),
(3, 'solucoes', '201505181329153.jpg', 'Trabalhamos com<br>grandes demandas para<br>posições temporárias,<br>terceirizadas e efetivas', 'We work with great demands <br>for temporary,<br>outsourced and <br>permanent positions', 2, '2015-05-08 21:43:03', '2015-06-10 23:00:01'),
(4, 'solucoes', '201505181329214.jpg', 'Visando uma<br>maior assertividade,<br>disponibilizamos aos<br>clientes célula<br>customizada', 'Seeking greater assertiveness,<br>we make customized <br>cells available to the client', 3, '2015-05-08 21:43:24', '2015-06-10 23:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_candidatos`
--

CREATE TABLE IF NOT EXISTS `novosite_candidatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oportunidades_id` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `observacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `curriculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `novosite_candidatos`
--

INSERT INTO `novosite_candidatos` (`id`, `oportunidades_id`, `nome`, `email`, `telefone`, `observacoes`, `curriculo`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '1@1.1', '1', '1', '11052015140245Nossos Diferenciais - Revisado.docx', NULL, '2015-05-11 17:02:45', '2015-05-11 17:02:45'),
(2, 1, '1', '1@1.1', '1', '1', '11052015140315Nossos Diferenciais - Revisado.docx', NULL, '2015-05-11 17:03:16', '2015-05-11 17:03:16'),
(3, 1, '1', '1@1.1', '1', '''', '11052015142529Executive Search - Revisado.docx', NULL, '2015-05-11 17:25:29', '2015-05-11 17:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_cases`
--

CREATE TABLE IF NOT EXISTS `novosite_cases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `novosite_cases`
--

INSERT INTO `novosite_cases` (`id`, `empresa`, `imagem`, `titulo_pt`, `titulo_en`, `texto_pt`, `texto_en`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'Bayer', '1505181604111.jpg', 'Grandes Demandas e Posições Temporárias e Efetivas', 'Big Demands and Temporary and Effective Positions', '<p><span style="color:#FFF">A Bayer &eacute; uma empresa com atua&ccedil;&atilde;o global nos setores de Sa&uacute;de (Bayer HealthCare), Agroneg&oacute;cios (Bayer CropScience) e Materiais Inovadores (Bayer MaterialScience) e conta com o Grupo Meta RH h&aacute;&nbsp;<strong>mais de 25 anos</strong>&nbsp;em processos de&nbsp;<strong>Recrutamento &amp; Sele&ccedil;&atilde;o</strong>, atendendo grandes demandas para suas plantas fabris Socorro e Cancioneiro (Bayer HealthCare) e em toda a contrata&ccedil;&atilde;o e administra&ccedil;&atilde;o de m&atilde;o de obra tempor&aacute;ria, assegurando todos os direitos da legisla&ccedil;&atilde;o vigente, al&eacute;m de agilidade nos processos de contrata&ccedil;&atilde;o.</span></p>\r\n\r\n<p><span style="color:#FFF">O Grupo Meta RH atua tamb&eacute;m com posi&ccedil;&otilde;es corporativas (tempor&aacute;rias e efetivas) para sua sede em S&atilde;o Paulo, atendendo perfis administrativos, t&eacute;cnicos e especializados, realizando processos totalmente&nbsp;<strong>focados nas compet&ecirc;ncias globais do Grupo Bayer</strong>&nbsp;e atrelados &agrave;s compet&ecirc;ncias exigidas para cada fun&ccedil;&atilde;o.&nbsp;<strong>Todo processo &eacute; personalizado</strong>&nbsp;e mapeado de acordo com a necessidade de cada gestor e &aacute;rea de neg&oacute;cio e pautado a partir do alto n&iacute;vel de entendimento e assertividade, que permite ao Grupo Meta RH solidificar a cada ano esta parceria.</span></p>\r\n', '<p><span style="color:#FFF">Bayer is a global enterprise with operations in the Health sector (Bayer HealthCare), Agribusiness (Bayer CropScience) and Innovative Materials (Bayer MaterialScience) and has counted on Grupo Meta RH for&nbsp;<strong>more than 25 years</strong>&nbsp;in&nbsp;<strong>Recruitment &amp; Selection</strong>&nbsp;processes, serving large demands for its manufacturing plants Socorro and Cancioneiro (Bayer HealthCare) and throughout the procurement and management of temporary labor, guaranteeing all the rights of the current legislation, as well as flexibility in procurement procedures.</span></p>\r\n\r\n<p><span style="color:#FFF">Grupo Meta RH also works with corporate positions (temporary and effective) for its headquarters in S&atilde;o Paulo, serving specialized, administrative and technical profiles, performing processes totally&nbsp;<strong>focused on global competencies of the Bayer Group</strong>&nbsp;and linked to the skills required for each role.&nbsp;<strong>The entire process is mapped and customized</strong>&nbsp;according to the need of each manager and business area and ruled from the high level of understanding and assertiveness, which allows Grupo Meta RH to solidify this partnership each year.</span></p>\r\n', 0, '2015-05-18 16:44:53', '2015-06-15 17:21:05'),
(2, 'Deloitte', '1505181607582.jpg', 'Atração de Talentos', 'Attraction of Talent', '<p><span style="color:#FFF">Empresa multinacional conceituada como a maior do mundo na &aacute;rea de servi&ccedil;os profissionais de Auditoria, Consultoria Tribut&aacute;ria e Consultoria em Gest&atilde;o de Riscos Empresariais, a Deloitte tem com o&nbsp;<strong>Grupo Meta RH</strong>&nbsp;uma&nbsp;<strong>parceria bem sucedida</strong>, confiando na nossa capacidade de recrutar e selecionar profissionais que s&atilde;o fundamentais para o sucesso de seus neg&oacute;cios, de modo que temos uma atua&ccedil;&atilde;o direta na&nbsp;<strong>atra&ccedil;&atilde;o de talentos</strong>&nbsp;para seu escrit&oacute;rio corporativo.</span></p>\r\n', '<p><span style="color:#FFF">Renowned multinational company as the world&#39;s largest professional services in the area of Audit, Tax Consultancy and Consulting in Enterprise Risk Management, Deloitte has in&nbsp;<strong>Grupo Meta RH</strong>&nbsp;a&nbsp;<strong>successful partnership</strong>, trusting in our ability to recruit and select professionals who are fundamental to the success of their business, so we have a direct role in&nbsp;<strong>attracting talent</strong>&nbsp;for their corporate office.</span></p>\r\n', 0, '2015-05-18 19:07:58', '2015-06-15 17:23:17'),
(3, 'Ecolab', '1505181609583.jpg', 'Posições Efetivas, Temporárias e Terceirizadas', 'Full-time, Temporary and Outsourced Positions', '<p><span style="color:#FFF">Empresa multinacional e l&iacute;der global em &aacute;gua, higiene, tecnologias de energia e servi&ccedil;os, a&nbsp;<strong>Ecolab</strong>&nbsp;tem com o&nbsp;<strong>Grupo Meta RH</strong>&nbsp;uma parceria bem sucedida h&aacute; mais de 30 anos, proporcionando&nbsp;<strong>excelentes resultados</strong>&nbsp;em diversos projetos para posi&ccedil;&otilde;es efetivas, tempor&aacute;rias e terceirizadas, dando suporte para todas as unidades de neg&oacute;cios, atendendo, assim, todas as suas demandas nacionais.</span></p>\r\n', '<p><span style="color:#FFF">Multinational company and global leader in water, hygiene, energy technologies and services,&nbsp;<strong>Ecolab&nbsp;</strong>has had with&nbsp;<strong>Grupo Meta RH</strong>&nbsp;a successful partnership for over 30 years, providing&nbsp;<strong>excellent results&nbsp;</strong>in several projects for full-time, temporary and outsourced positions, supporting all the business units, serving, thus, all their national demands.</span></p>\r\n', 0, '2015-05-18 19:09:58', '2015-06-15 17:25:36'),
(4, 'Grupo Fleury', '1505181611574.jpg', 'Grandes Demandas e Posições Pontuais', 'Great Demands and Punctual Positions', '<p><span style="color:#FFF">Com uma equipe exclusiva e focada na &aacute;rea de Sa&uacute;de para o atendimento &agrave; demanda de posi&ccedil;&otilde;es, nossa parceria permite uma troca de aprendizado nesta &aacute;rea. Hoje somos um dos&nbsp;<strong>principais fornecedores&nbsp;</strong>de solu&ccedil;&otilde;es de c&eacute;lulas para o recrutamento e sele&ccedil;&atilde;o de seus profissionais.&nbsp;<strong>Customizamos uma c&eacute;lula de atendimento</strong>&nbsp;focada na contrata&ccedil;&atilde;o de toda a sua opera&ccedil;&atilde;o e posi&ccedil;&otilde;es estrat&eacute;gicas.</span></p>\r\n', '<p><span style="color:#FFF">With a unique and focused team in the area of Health to meet the demand of positions, our partnership allows an exchange of learning in this area. Today we are a&nbsp;<strong>leading supplier&nbsp;</strong>of cell solutions for recruitment and selection of its professionals.&nbsp;<strong>We customize a service cell&nbsp;</strong>focused on hiring its entire operation and strategic positions.</span></p>\r\n', 0, '2015-05-18 19:11:57', '2015-05-18 19:11:57'),
(5, 'Libbs', '1505181612445.jpg', 'Demandas Emergenciais', 'Emergency Demands', '<p><span style="color:#FFF">Em novembro de 2011 a LIBBS confiou um desafio ao Grupo Meta RH:&nbsp;<strong>contratar em prazo recorde 105 colaboradores</strong>&nbsp;para abertura do terceiro turno em sua unidade fabril. Foi&nbsp;<strong>criada uma c&eacute;lula de atendimento espec&iacute;fica</strong>&nbsp;com o objetivo de atender a demanda e desenvolver estrat&eacute;gias para encontrar os&nbsp;<strong>melhores profissionais</strong>&nbsp;que hoje fazem parte do quadro de colaboradores da empresa.</span></p>\r\n', '<p><span style="color:#FFF">In November 2011 LIBBS entrusted a challenge to the Grupo Meta RH:&nbsp;<strong>hiring 105 employees in record time</strong>&nbsp;for the opening of a third shift at its manufacturing unit. A&nbsp;<strong>specific service cell was created</strong>&nbsp;in order to meet the demand and develop strategies to find the&nbsp;<strong>best professionals</strong>&nbsp;that are now part of the company staff.</span></p>\r\n', 0, '2015-05-18 19:12:44', '2015-05-18 19:12:44'),
(6, 'MSD', '1505181614036.jpg', 'Célula Customizada', 'Customized Cell', '<p><span style="color:#FFF"><strong>Ampliando parceria</strong>&nbsp;de sucesso, o grupo Merck Sharp Dohme escolheu o&nbsp;<strong>Grupo Meta RH</strong>&nbsp;como&nbsp;<strong>principal fornecedor</strong>&nbsp;para atender suas demandas estrat&eacute;gicas de&nbsp;<strong>profissionais qualificados</strong>.</span></p>\r\n\r\n<p><span style="color:#FFF">Uma&nbsp;<strong>c&eacute;lula dedicada</strong>&nbsp;e alinhada com as estrat&eacute;gias de busca de talentos humanos do cliente atende a MSD com&nbsp;<strong>alta assertividade e parceria</strong>&nbsp;agregadora de valores.</span></p>\r\n', '<p><span style="color:#FFF"><strong>Expanding successful partnership</strong>, the Merck Sharp Dohme group chose&nbsp;<strong>Grupo Meta RH&nbsp;</strong>as&nbsp;<strong>main supplier</strong>&nbsp;to meet their strategic demands for&nbsp;<strong>skilled professionals</strong>.</span></p>\r\n\r\n<p><span style="color:#FFF">A&nbsp;<strong>dedicated cell</strong>, aligned with the client&rsquo;s search strategies for human talents serves MSD with&nbsp;<strong>high assertiveness and partnership</strong>&nbsp;aggregating values.</span></p>\r\n', 0, '2015-05-18 19:14:03', '2015-05-18 19:14:03'),
(7, 'NET', '1505181614427.jpg', 'Célula Customizada', 'Customized Cell', '<p><span style="color:#FFF">A NET &eacute; uma empresa de&nbsp;<em>triple play</em>&nbsp;brasileira com forte atua&ccedil;&atilde;o no segmento de telecomunica&ccedil;&otilde;es, oferecendo servi&ccedil;os como televis&atilde;o por assinatura, internet banda larga e telefonia VoIP. H&aacute; mais de 5 anos tem&nbsp;<strong>confiado</strong>&nbsp;ao&nbsp;<strong>Grupo Meta RH</strong>, atrav&eacute;s da divis&atilde;o BPO, a responsabilidade de recrutar profissionais tanto da &aacute;rea t&eacute;cnica como da corporativa, em que&nbsp;<strong>oferecemos a solu&ccedil;&atilde;o</strong>&nbsp;de&nbsp;<strong>c&eacute;lula customizada</strong>&nbsp;com total foco nas necessidades do cliente.</span></p>\r\n', '<p><span style="color:#FFF">NET is a Brazilian triple play company with a strong performance in the telecommunications segment, offering services such as pay-TV, broadband internet and VoIP telephony services. For more than five years it has&nbsp;<strong>entrusted&nbsp;</strong>to&nbsp;<strong>Grupo Meta RH</strong>, through its BPO division, the responsibility to recruit professionals from both the technical and corporate areas, in which we offer the customized solution cell with total focus on customer needs.</span></p>\r\n', 0, '2015-05-18 19:14:42', '2015-05-18 19:14:42'),
(8, 'Ranbaxy', '1505181616068.jpg', 'Demandas de Temporários e Terceiros', 'Temporary and Outsourced Demands', '<p><span style="color:#FFF">A s&oacute;lida parceria de muitos anos entre Ranbaxy e&nbsp;<strong>Grupo Meta RH</strong>&nbsp;tem proporcionado&nbsp;<strong>excelentes resultados</strong>&nbsp;em diversos projetos de tempor&aacute;rios e terceiros com o objetivo de atender&nbsp;<strong>demandas emergenciais</strong>&nbsp;para o escrit&oacute;rio corporativo localizado em S&atilde;o Paulo e para sua planta fabril localizada na cidade de S&atilde;o Gon&ccedil;alo-RJ.</span></p>\r\n', '<p><span style="color:#FFF">The solid partnership of many years between Ranbaxy and&nbsp;<strong>Grupo Meta RH</strong>&nbsp;has provided&nbsp;<strong>excellent results</strong>&nbsp;in various temporary and outsourced projects aiming to meet&nbsp;<strong>emergency demands</strong>&nbsp;for corporate office located in S&atilde;o Paulo and its manufacturing plant in the city of S&atilde;o Gon&ccedil;alo- RJ.</span></p>\r\n', 0, '2015-05-18 19:16:06', '2015-05-18 19:16:06'),
(9, 'Serasa Experian', '1505181617009.jpg', 'Célula Customizada', 'Customized Cell', '<p><span style="color:#FFF">Buscando atender com maior&nbsp;<strong>assertividade</strong>&nbsp;a Serasa Experian, um dos maiores bir&ocirc;s de cr&eacute;dito do mundo,&nbsp;<strong>a Meta BPO</strong>, oferece uma&nbsp;<strong>estrutura alinhada e dedicada</strong>&nbsp;&agrave; estrat&eacute;gia de&nbsp;<strong><em>Talent Aquisition</em></strong>&nbsp;do cliente, em&nbsp;<strong>atua&ccedil;&atilde;o direta com os gestores</strong>&nbsp;requisitantes de cada &aacute;rea, em parceria que tem mostrado&nbsp;<strong>excelentes resultados</strong>.</span></p>\r\n', '<p><span style="color:#FFF">Seeking to serve with greater&nbsp;<strong>assertiveness&nbsp;</strong>Serasa Experian, one of the largest credit bureaus in the world,&nbsp;<strong>Meta BPO</strong>&nbsp;offers a structure dedicated and aligned to the client&rsquo;s&nbsp;<strong>Talent Acquisition</strong>&nbsp;strategy in&nbsp;<strong>direct work with the requesting managers</strong>&nbsp;in each area, in partnership that has shown&nbsp;<strong>excellent results</strong>.</span></p>\r\n', 0, '2015-05-18 19:17:01', '2015-06-15 17:27:25'),
(10, 'Unilever', '15051816174610.jpg', 'Célula Customizada', 'Customized Cell', '<p><span style="color:#FFF">Para esse projeto disponibilizamos o&nbsp;<strong>atendimento BPO</strong>, contando com uma equipe alinhada e dedicada &agrave;s estrat&eacute;gias de&nbsp;<strong>Talent Aquisition</strong>&nbsp;da Unilever. Esta equipe foi treinada no sistema de requisi&ccedil;&atilde;o de vagas da empresa e tem&nbsp;<strong>atua&ccedil;&atilde;o direta com gestores</strong>&nbsp;requisitantes de cada &aacute;rea, tornando-nos assim um dos seus principais fornecedores em solu&ccedil;&otilde;es para RH.</span></p>\r\n', '<p><span style="color:#FFF">For this project we provide&nbsp;<strong>BPO service</strong>, with an aligned team dedicated to the Unilever&nbsp;<strong>Talent Acquisition</strong>&nbsp;strategies. This team was trained in the company&rsquo;s the job requisition system and has&nbsp;<strong>direct operations with requesting managers</strong>&nbsp;in each area, thus making us one of its main suppliers of HR solutions.</span></p>\r\n', 0, '2015-05-18 19:17:46', '2015-06-15 17:28:13');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_contato`
--

CREATE TABLE IF NOT EXISTS `novosite_contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `novosite_contato`
--

INSERT INTO `novosite_contato` (`id`, `telefone`, `facebook`, `twitter`, `linkedin`, `endereco`, `google_maps`, `created_at`, `updated_at`) VALUES
(1, '+55 11 5525-2711', 'https://www.facebook.com/metabpo1', 'https://twitter.com/MetaBPO_', 'https://www.linkedin.com/company/meta-bpo', '<p>Av. Adolfo Pinheiro, 1001<br />\r\nSanto Amaro<br />\r\n04733-100 &bull; S&atilde;o Paulo/SP</p>\r\n', '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?q=Av.+Adolfo+Pinheiro,+1001&ie=UTF8&hq=&hnear=Avenida+Adolfo+Pinheiro,+1001+-+Santo+Amaro,+S%C3%A3o+Paulo,+04733-200&gl=br&t=m&z=14&ll=-23.644777,-46.700475&output=embed"></iframe>', '0000-00-00 00:00:00', '2015-05-26 20:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_depoimentos`
--

CREATE TABLE IF NOT EXISTS `novosite_depoimentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `autoria_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `autoria_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `novosite_depoimentos`
--

INSERT INTO `novosite_depoimentos` (`id`, `empresa`, `titulo_pt`, `titulo_en`, `texto_pt`, `texto_en`, `autoria_pt`, `autoria_en`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'Libbs', 'Projeto de Expansão', 'Expansion Project', '<p>&ldquo;O Grupo Meta RH &eacute; nosso parceiro h&aacute; muitos anos e temos obtido bons resultados em processos seletivos, sobretudo em projetos espec&iacute;ficos. Em 2012, contratamos a Meta BPO para o recrutamento e sele&ccedil;&atilde;o de 132 tempor&aacute;rios e a Meta conseguiu conciliar tr&ecirc;s pontos que para n&oacute;s eram imprescind&iacute;veis: volume de vagas, prazo e qualidade. Estamos satisfeitos com esta parceria.&rdquo;</p>\r\n', '<p><span style="color:rgb(102, 102, 102)">&ldquo;</span>Grupo Meta RH<span style="color:rgb(102, 102, 102)">&nbsp;has been our partner for many years and we have obtained good results in selection processes, especially in specific projects. In 2012, we hired Meta BPO for recruitment and selection of 132 temporary employees and Meta managed to reconcile the three points that were essential for us: volume of vacancies, time and quality. We are pleased with this partnership.&rdquo;</span></p>\r\n', 'Simone Figueiredo, Recursos Humanos, Libbs Farmacêutica.', 'Simone Figueiredo, Human Resources, Libbs Farmacêutica.', '20150518133309libbs.jpg', 0, '2015-05-18 16:31:27', '2015-05-18 19:20:10'),
(2, 'Ranbaxy', 'Demandas de Temporários e Terceiros', 'Demands for Temporary and Outsourced Employees', '<p>&ldquo;O Grupo Meta RH tem um padr&atilde;o que para n&oacute;s j&aacute; faz parte da vida da Ranbaxy, h&aacute; mais de 7 anos. Um trabalho de grande relev&acirc;ncia foi o suporte na contrata&ccedil;&atilde;o de pessoal efetivo e/ou tempor&aacute;rio para nossa unidade de S&atilde;o Gon&ccedil;alo (RJ), ainda que sem uma eventual base instalada na cidade. O Grupo Meta RH entregou o resultado esperado, sem nenhum custo adicional ou reclama&ccedil;&otilde;es dos clientes internos. Este &eacute; o grande diferencial: entrega do esperado, execu&ccedil;&atilde;o em 100% do que foi prometido, nada de passivo trabalhista nos mais de 20 anos que j&aacute; trabalho com a Meta.&rdquo;</p>\r\n', '<p><span style="color:rgb(102, 102, 102)">&ldquo;</span>Grupo Meta RH<span style="color:rgb(102, 102, 102)">&nbsp;has a standard that for us is already part of the Ranbaxy life for over 7 years. A work of great importance was the support in hiring effective and/or temporary staff for our S&atilde;o Gon&ccedil;alo (RJ) unit, albeit without any installed base in the city.&nbsp;</span>Grupo Meta RH<span style="color:rgb(102, 102, 102)">&nbsp;delivered the expected result, with no additional cost or complaints from internal customers. This is the big difference: delivering the expected, 100% execution of what was promised, no labor liabilities in over 20 years that I&#39;ve worked with Meta.&rdquo;</span></p>\r\n', 'Cândido Barbosa, HR Latam Director na empresa Ranbaxy.', 'Cândido Barbosa, HR Latam Director at the Ranbaxy company.', '20150518133422ranbaxy.jpg', 0, '2015-05-18 16:34:22', '2015-05-18 19:20:34'),
(3, 'Serasa Experian', 'Recrutamento e Seleção', 'Recruitment and Selection', '<p>&ldquo;Confiamos &agrave; Meta BPO, al&eacute;m de diversas vagas, muitos projetos de &acirc;mbito nacional. Sempre pudemos contar com consultores que nos atenderam com parceria e efici&ecirc;ncia, selecionando candidatos de diversos n&iacute;veis e atendendo nossas demandas nos prazos acordados. Trabalhamos em parceria com a META, que nos auxilia e acompanha no que for necess&aacute;rio, com sua equipe proativa e engajada.&rdquo;</p>\r\n', '<p><span style="color:rgb(102, 102, 102)">&ldquo;We trust&nbsp;</span>Meta BPO<span style="color:rgb(102, 102, 102)">, beyond several positions, many national scope projects. We always have been able to count on consultants who served us with the partnership and efficiency, selecting candidates on different levels and serving our demands within the deadline agreed upon. We work in partnership with META, which assists and accompanies us as necessary, with its proactive and engaged team.&rdquo;</span></p>\r\n', 'Natalia Leoni, Atração de Talentos da Serasa Experian.', 'Natalia Leoni, Talent Attraction at Serasa Experian.', '20150518133526serasa.jpg', 0, '2015-05-18 16:35:26', '2015-05-18 19:21:07'),
(4, 'Varian', 'Recrutamento e Seleção e Terceirização', 'Recruitment and Selection and Outsourcing', '<p>&ldquo;H&aacute; mais de 10 anos a Varian mant&eacute;m relacionamento com o Grupo Meta RH, que nos presta servi&ccedil;os de terceiriza&ccedil;&atilde;o e recrutamento e sele&ccedil;&atilde;o. Neste per&iacute;odo pudemos acompanhar o crescimento quantitativo e qualitativo da empresa, preservando sempre os fundamentos de qualidade de atendimento ao cliente, bem como a qualidade dos servi&ccedil;os prestados, pre&ccedil;os justos e o comprometimento com as demandas dos nossos clientes internos.&rdquo;</p>\r\n', '<p><span style="color:rgb(102, 102, 102)">&ldquo;For over 10 years, Varian has maintained relationships with&nbsp;</span>Grupo Meta RH&nbsp;<span style="color:rgb(102, 102, 102)">which provides us services in outsourcing and recruitment and selection. In this period, we were able to monitor the quantitative and qualitative growth of the company, while preserving the fundamentals of quality customer service and the quality of services, fair prices and a commitment to the needs of our internal customers.&rdquo;</span></p>\r\n', 'Celso Rosa da Silva, diretor financeiro, Varian Medical Systems Brasil.', 'Celso Rosa da Silva, financial director, Varian Medical Systems Brasil.', '20150518133620varian.jpg', 0, '2015-05-18 16:36:20', '2015-06-15 17:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_diferenciais`
--

CREATE TABLE IF NOT EXISTS `novosite_diferenciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `novosite_diferenciais`
--

INSERT INTO `novosite_diferenciais` (`id`, `texto_pt`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, '<p>Com grande know-how adquirido em mais de 30 anos no <strong>Recrutamento &amp; Sele&ccedil;&atilde;o</strong> de profissionais para as &aacute;reas operacional, administrativa, t&eacute;cnica e especializada, a <strong>Meta BPO </strong>possui uma trajet&oacute;ria de sucesso neste mercado, com mais de 1.000 clientes atendidos, alguns com mais de 25 anos de relacionamento, al&eacute;m de ter inserido mais de 100.000 profissionais no mercado de trabalho.</p>\r\n\r\n<p><span style="font-size:20px"><span style="color:#62BBB1">Possu&iacute;mos um atendimento exclusivo e sob medida para atender &agrave;s necessidades de cada cliente, com c&eacute;lulas dedicadas, disponibilizando consultores especialistas, que atuar&atilde;o de maneira consultiva e com foco total no desenvolvimento dos neg&oacute;cios de nossos clientes.</span></span></p>\r\n', '<p><span style="font-size:10pt">With great know-how acquired in over 30 years in the <strong>Recruiting &amp; Selection </strong>of professionals for operational, administrative, technical and specialized areas, <strong>Meta BPO </strong>has a path of success in this Market, with over 1,000 clients served, some with over 25 years of relationship, besides having inserted more than 100,000 professionals in the work market.</span></p>\r\n\r\n<p><span style="font-size:20px"><span style="color:#62BBB1">We have exclusive, tailored service to serve the needs of each client, with dedicated cells, making available expert consultants, who will act in a consulting manner with total focus on the development of our clients&rsquo; business.</span></span></p>\r\n', '0000-00-00 00:00:00', '2015-06-10 22:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_oportunidades`
--

CREATE TABLE IF NOT EXISTS `novosite_oportunidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `n_posicoes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_cadastro` date NOT NULL,
  `data_expiracao` date DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `idioma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `novosite_oportunidades`
--

INSERT INTO `novosite_oportunidades` (`id`, `empresa`, `titulo`, `slug`, `n_posicoes`, `cidade`, `estado`, `data_cadastro`, `data_expiracao`, `texto`, `idioma`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1dsdasdas', '1dasdasda', '1-1dasdasda', '12 Posições', '1dasdd', '1a', '2015-05-04', NULL, '<p>1</p>\r\n', 'pt', NULL, '2015-05-11 17:01:12', '2015-05-11 17:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_privacidade`
--

CREATE TABLE IF NOT EXISTS `novosite_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `novosite_quem_somos`
--

CREATE TABLE IF NOT EXISTS `novosite_quem_somos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `novosite_quem_somos`
--

INSERT INTO `novosite_quem_somos` (`id`, `texto_pt`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, '<p>A<strong> Meta BPO</strong> &eacute; a divis&atilde;o do Grupo Meta RH especializada em prover solu&ccedil;&otilde;es em <strong>Recrutamento &amp; Sele&ccedil;&atilde;o</strong>, <strong>Gest&atilde;o de Profissionais Tempor&aacute;rios</strong> e <strong>Outsourcing</strong>, desenvolvendo processos alinhados &agrave;s necessidades dos clientes, com a solidez de mais de 30 anos no mercado de Recursos Humanos.</p>\r\n\r\n<p><span style="font-size:20px"><span style="color:#62BBB1">A Meta BPO agrega &agrave; sua experi&ecirc;ncia, agilidade e profissionalismo, al&eacute;m de possuir profissionais altamente capacitados, com foco em atender clientes dos mais diversos segmentos de maneira personalizada, o que possibilita desenvolver parcerias eficazes e duradouras.</span></span></p>\r\n', '<p><strong>Meta BPO </strong>is a division of Grupo Meta RH specialized in providing solutions in <strong>Recruiting &amp; Selection</strong>, <strong>Management of Temporary Professionals </strong>and <strong>Outsourcing</strong>, developing processes aligned to the clients&rsquo; needs, with the solidity of over 30 years in the Human Resources market.</p>\r\n\r\n<p><span style="font-size:20px"><span style="color:#62BBB1">Meta BPO adds its experience, agility and professionalism, besides having highly qualified professionals, with focus on serving the clients from the most varied segments in a personalized manner, which allows for the development of efficient, long-lasting partnerships.</span></span></p>\r\n', '0000-00-00 00:00:00', '2015-06-10 22:44:27');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_solucoes`
--

CREATE TABLE IF NOT EXISTS `novosite_solucoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `novosite_solucoes`
--

INSERT INTO `novosite_solucoes` (`id`, `slug`, `titulo_pt`, `titulo_en`, `texto_pt`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, 'recrutamento-e-selecao', 'Recrutamento & Seleção', 'Recruitment & Selection', '<ul>\r\n	<li>Experi&ecirc;ncia no atendimento de posi&ccedil;&otilde;es pontuais e projetos de grandes demandas;</li>\r\n	<li>Banco de dados com mais de 700.000 profissionais;</li>\r\n	<li>Processos customizados e alinhados &agrave; cultura empresarial e &agrave;s necessidades da empresa (c&eacute;lula dedicada);</li>\r\n	<li>Atendimento personalizado aos clientes e candidatos, por meio de feedbacks constantes ao longo de todo o processo seletivo.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Experience in the fulfillment of punctual positions and projects of great demands;</li>\r\n	<li>Database with over 700,000 professionals;</li>\r\n	<li>Personalized processes, aligned to the company&rsquo;s business culture and the needs (dedicated cell);</li>\r\n	<li>Personalized service for the clients and candidates, through constant feedbacks throughout the entire selective process.</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-06-10 22:51:00'),
(2, 'temporarios', 'Temporários', 'Temps', '<ul>\r\n	<li>Processos de Recrutamento, Sele&ccedil;&atilde;o e Administra&ccedil;&atilde;o de profissionais tempor&aacute;rios para atender projetos espec&iacute;ficos ou sazonais dos clientes, nos termos da Lei 6019/1974;</li>\r\n	<li>Transpar&ecirc;ncia, idoneidade e &eacute;tica na administra&ccedil;&atilde;o, comprovadas pelos nossos clientes;</li>\r\n	<li>Op&ccedil;&atilde;o de contrata&ccedil;&atilde;o sem custos de recrutamento e sele&ccedil;&atilde;o.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Temporary professionals Recruiting, Selection and Administration Processes to fulfill specific or seasonal projects, in terms of Law 6019/1974;</li>\r\n	<li>Transparency, honesty and ethics in administration, proven by our clients;</li>\r\n	<li>Contracting options without recruiting and selection costs.</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-06-10 22:51:29'),
(3, 'outsourcing-bpo', 'Outsourcing | BPO', 'Outsourcing | BPO', '<ul>\r\n	<li><strong>Dedicado</strong>: Profissionais devidamente capacitados que atender&atilde;o de forma personalizada a um &uacute;nico cliente;</li>\r\n	<li><strong>On Demand</strong>: Processo direcionado a projetos pontuais, com prazos e custos previamente definidos;</li>\r\n	<li><strong>On Site</strong>: Processo direcionado a projetos personalizados que ser&atilde;o executados utilizando o local, infraestrutura e ferramentas do cliente;</li>\r\n	<li><strong>Off Site</strong>: Processo direcionado a projetos que ser&atilde;o realizados, utilizando a infraestrutura e ferramentas da Meta BPO.</li>\r\n</ul>\r\n', '<ul>\r\n	<li><strong>Dedicated</strong>: Duly qualified professionals who will fulfill in a personalized manner for a single client;</li>\r\n	<li><strong>On Demand</strong>: Process directed towards punctual projects, with terms and costs previously defined;</li>\r\n	<li><strong>On Site</strong>: Process directed to the personalized projects that will be executed using the client&rsquo;s location, infrastructure and tools;</li>\r\n	<li><strong>Off Site</strong>: Process directed to the projects that will be carried out using Meta BPO&rsquo;s tools and infrastructure.</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-06-10 22:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `novosite_usuarios`
--

CREATE TABLE IF NOT EXISTS `novosite_usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `novosite_usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `novosite_usuarios`
--

INSERT INTO `novosite_usuarios` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$6$rounds=5000$l1DewsmAEX./FZNP$Z2oXOliKws/guKEc899Wag8R5iWKC1kYCDexn4N9rhFz8mqo.7e46QZklMui8XWxE0MlgUWIdkXxOB8t2wcmB0', 'LGg7n46sDOEjnK9YnN5Y2ogupL797zTyImCp47TbFOU4AzDOYxfFZOlxMZQz', '0000-00-00 00:00:00', '2015-09-11 17:21:26'),
(2, 'meta', 'contato@metabpo.com.br', '$6$rounds=5000$C86n0GTpdULHs3ek$Tykh0VjbPgL31xVzcj/H88JAK82NhYfYiFfdeTAM7evh3bVzUAt0VQga2ef/eBAYXeShuTUDhp4JRzbgvvfgT.', '', '2015-05-22 18:52:31', '2015-05-22 18:52:31'),
(3, 'vinicius', 'viniciusruffato@grupometarh.com.br', '$6$rounds=5000$U9.bFdalhdY7HQXj$knuUdlCtTedVhGv6kOawmHM0DKo/vgYeDEIUBpr1Ggbed109kLmRivzCtM02JC1FlME2Au.UA6PSaFjEEnFBo1', '', '2015-05-26 21:10:22', '2015-05-26 21:10:22');

-- --------------------------------------------------------

--
-- Table structure for table `programas`
--

CREATE TABLE IF NOT EXISTS `programas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `programa_categoria` varchar(255) NOT NULL,
  `programa_imagem` varchar(255) NOT NULL,
  `programa_empresa` varchar(255) NOT NULL,
  `programa_link` varchar(255) NOT NULL,
  `programa_data_cadastro` int(10) NOT NULL,
  `programa_data_expiracao` int(255) NOT NULL,
  `programa_destaque` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 1),
(2, 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'trupe', '$P$BoHcs.tjonTtZ1E1MY4lhA0HLRDX2X0', 'nilton@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '189.40.38.138', '2015-05-20 15:24:24', '2012-11-21 15:58:02', '2015-05-20 18:24:24'),
(2, 'metabpo', '$P$Bv53BXgU2NsN1R43T8dzFkhqDM.2Ug/', 'niltondefreitas@gmail.com', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '200.159.74.242', '2015-05-20 10:06:42', '2013-01-14 09:39:34', '2015-05-20 13:06:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_autologin`
--

INSERT INTO `user_autologin` (`key_id`, `user_id`, `user_agent`, `last_ip`, `last_login`) VALUES
('2c09f0640c3db14757e5a61fd8644c5f', 2, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.56 Safari/537.17', '200.159.74.242', '2013-01-31 15:06:49'),
('411c0d49c7479f6333cb3457273321b0', 2, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0', '200.159.74.242', '2013-02-04 18:31:53'),
('44cb6d388386b4939e30b298f7a051ef', 2, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.154 Safari/537.36', '200.159.74.242', '2014-09-15 16:02:03'),
('6cd07fcc513bdcd6fb0e60cf09f401ae', 1, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36', '177.148.180.69', '2015-03-31 14:34:33'),
('a64edbf7841aeef7d9443689ceb524ee', 2, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)', '200.159.74.242', '2013-06-20 14:28:20'),
('c6bb8d5e86ae1b5282dea65039a44524', 2, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; BTRS105626; OfficeLiveConnector.1.3; OfficeLivePatch.0.0; .NET CLR 3.0.4506.2152; .NE', '200.159.74.242', '2013-06-20 16:29:18'),
('db6dd4626e0a381e8c80c860e3a9d593', 2, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.57 Safari/537.36', '200.159.74.242', '2013-08-29 20:40:44'),
('dd53b626ff9e5c4eccd27f221385fc32', 2, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36', '187.121.112.109', '2013-10-23 15:11:19');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vagas`
--

CREATE TABLE IF NOT EXISTS `vagas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vaga_categoria` varchar(255) NOT NULL,
  `vaga_imagem` varchar(255) DEFAULT NULL,
  `vaga_empresa` varchar(255) NOT NULL,
  `vaga_titulo` varchar(255) NOT NULL,
  `vaga_descritivo` text NOT NULL,
  `vaga_quantidade` int(10) NOT NULL,
  `vaga_link` varchar(255) NOT NULL,
  `vaga_data_cadastro` int(10) NOT NULL,
  `vaga_data_expiracao` int(255) NOT NULL,
  `vaga_destaque` tinyint(1) NOT NULL,
  `vaga_local` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=173 ;

--
-- Dumping data for table `vagas`
--

INSERT INTO `vagas` (`id`, `vaga_categoria`, `vaga_imagem`, `vaga_empresa`, `vaga_titulo`, `vaga_descritivo`, `vaga_quantidade`, `vaga_link`, `vaga_data_cadastro`, `vaga_data_expiracao`, `vaga_destaque`, `vaga_local`) VALUES
(169, '', NULL, 'Segmento químico', 'Analista de Homologação ', '<table id="Pr2C" style="width: 100%; height: 65%;" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor="#FFFFFF">\n<tbody>\n<tr>\n<td colspan="3" align="left" valign="top" width="80%">\n<table id="Pr2C" style="width: 100%; height: 65%;" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor="#FFFFFF">\n<tbody>\n<tr>\n<td colspan="3" align="left" valign="top" width="80%">\n<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta BPO &eacute; a divis&atilde;o especializada em Recrutamento e Sele&ccedil;&atilde;o, Tempor&aacute;rios e Outsourcing que vem desenvolvendo processos alinhados &agrave;s necessidades dos clientes e com a experi&ecirc;ncia de consultores altamente qualificados para atender organiza&ccedil;&otilde;es dos mais diversos segmentos.&nbsp;<br /><br />Estamos assessorando empresa multinacional, de grande porte do segmento qu&iacute;mico localizada na regi&atilde;o de Barueri, na contrata&ccedil;&atilde;o de:</p>\n<br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Analista de Homologa&ccedil;&atilde;o</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Organiza&ccedil;&atilde;o de documenta&ccedil;&atilde;o para homologa&ccedil;&atilde;o.<br /><br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Superior Completo;&nbsp;<br />&bull; Inform&aacute;tica: Excel intermedi&aacute;rio;&nbsp;<br />&bull; Idiomas: N&atilde;o se Aplica;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: Experi&ecirc;ncia com homologa&ccedil;&atilde;o.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Sal&aacute;rio: Compat&iacute;vel com o mercado;<br />&bull; Benef&iacute;cios: vale transporte, vale refei&ccedil;&atilde;o;<br />&bull; Hor&aacute;rio: das 8h00 &agrave;s 17h00;<br />&bull; Local de trabalho: Barueri;<br />&bull; Modalidade contratual: tempor&aacute;rio (6 meses);<br />&bull; Outros: disponibilidade para viagens nacionais.</span></td>\n<td id="LtC" align="left" valign="middle" bgcolor="#3064C8" width="2%">&nbsp;</td>\n</tr>\n</tbody>\n</table>\n</td>\n<td id="LtC" align="left" valign="middle" bgcolor="#3064C8" width="2%">&nbsp;</td>\n</tr>\n</tbody>\n</table>', 6, 'https://www.vagas.com.br/v1171143', 1430160456, 1433041200, 1, 'Barueri, SP.'),
(170, '', NULL, 'Segmento médico hospitalar', 'Técnico de Serviços Técnicos I ', '<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta BPO &eacute; a divis&atilde;o especializada em Recrutamento e Sele&ccedil;&atilde;o, Tempor&aacute;rios e Outsourcing que vem desenvolvendo processos alinhados &agrave;s necessidades dos clientes e com a experi&ecirc;ncia de consultores altamente qualificados para atender organiza&ccedil;&otilde;es dos mais diversos segmentos.&nbsp;<br /><br />Estamos assessorando empresa multinacional, de grande porte, do segmento m&eacute;dico-hospitalar localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<p><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>T&eacute;cnico de Servi&ccedil;os T&eacute;cnicos I</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Manuten&ccedil;&atilde;o preventiva e corretiva em bombas de infus&atilde;o;<br />&bull; Troca de pe&ccedil;as, calibra&ccedil;&atilde;o de equipamentos e execu&ccedil;&atilde;o de testes de rotina conforme procedimentos t&eacute;cnicos;<br />&bull; Manter a atualiza&ccedil;&atilde;o e organiza&ccedil;&atilde;o dos documentos e procedimentos t&eacute;cnicos pertinentes, por meio do preenchimento de formul&aacute;rios manuais e informatizados, em conformidade com as boas pr&aacute;ticas de documenta&ccedil;&atilde;o;<br />&bull; Zelar pelos equipamentos, recursos e materiais, sob sua responsabilidade, e pelo ambiente de trabalho.<br /><br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: T&eacute;cnico completo em Eletr&ocirc;nica, Mecatr&ocirc;nica ou cursos correlatos;&nbsp;<br />&bull; Inform&aacute;tica: bons conhecimentos do Pacote Office;&nbsp;<br />&bull; Idiomas: ingl&ecirc;s t&eacute;cnico para leitura de manuais;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: necess&aacute;rio ter experi&ecirc;ncia em manuten&ccedil;&atilde;o de equipamentos m&eacute;dico-hospitalares.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Sal&aacute;rio: compat&iacute;vel com o mercado;<br />&bull; Benef&iacute;cios: vale transporte e restaurante no local;<br />&bull; Hor&aacute;rio: das 8h00 &agrave;s 17h00;<br />&bull; Local de trabalho: zona sul de S&atilde;o Paulo, pr&oacute;ximo ao antigo hospital Santa Catarina;<br />&bull; Modalidade contratual: tempor&aacute;rio (6 meses);<br />&bull; Outros: com possibilidade de efetiva&ccedil;&atilde;o.</span></p>', 3, 'https://www.vagas.com.br/v1163985', 1430160519, 1433041200, 1, 'São Paulo, SP.'),
(171, '', NULL, 'Segmento farmacêutico', 'Analista Microbiológico Pleno ', '<table id="Pr2C" style="width: 100%; height: 65%;" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor="#FFFFFF">\n<tbody>\n<tr>\n<td colspan="3" align="left" valign="top" width="80%">\n<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta BPO &eacute; a divis&atilde;o especializada em Recrutamento e Sele&ccedil;&atilde;o, Tempor&aacute;rios e Outsourcing que vem desenvolvendo processos alinhados &agrave;s necessidades dos clientes e com a experi&ecirc;ncia de consultores altamente qualificados para atender organiza&ccedil;&otilde;es dos mais diversos segmentos.&nbsp;<br /><br />Estamos assessorando empresa multinacional, de grande porte, do segmento farmac&ecirc;utico localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Analista Microbiol&oacute;gico Pleno</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Auxiliar e/ou realizar as an&aacute;lises de degermantes, bioburden, &aacute;gua, contagem de particulas l&iacute;quidas, contagem de particulas vis&iacute;veis, cianocobalamina, microbiana e pesquisa de pat&ocirc;genos, pirog&ecirc;nio in vitro da &aacute;rea est&eacute;reis (produto acabado, &aacute;gua, mat&eacute;ria-prima), esterilidade nos produtos est&eacute;reis, monitoramento ambiental das &aacute;reas produtivas e laborat&oacute;rio microbiol&oacute;gico, indentifica&ccedil;&atilde;o microbiana e colora&ccedil;&atilde;o de gram e testes de fertilidade e esterilidade em meios de cultura e nos procedimentos de Media Fill;<br />&bull; Preparar materiais, meio de cultura e fluidos.<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Completa em Farm&aacute;cia, Qu&iacute;mica ou Biologia;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s Intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: Laborat&oacute;rio Microbiol&oacute;gico de Ind&uacute;strias Farmac&ecirc;utica, Cosm&eacute;tica ou Aliment&iacute;cia. Conhecimentos: BPL, Preparo de Meios de Cultura, Monitoramento Ambiental, Contagem de bact&eacute;rias e fungos, Pesquisa de Pat&oacute;genos, Teste de Promo&ccedil;&atilde;o de Crescimento, Teste de Esterilidade, USP e RDC 17.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Sal&aacute;rio: compat&iacute;vel com o mercado;<br />&bull; Benef&iacute;cios: vale transporte, refeit&oacute;rio no local, assist&ecirc;ncia m&eacute;dica, assist&ecirc;ncia odontol&oacute;gica e seguro de vida;<br />&bull; Hor&aacute;rio: Flexibilidade para os 2 turnos da empresa;&nbsp;<br />&bull; Local de trabalho: zona Sul de S&atilde;o Paulo;<br />&bull; Modalidade contratual: tempor&aacute;rio.</span></td>\n<td id="LtC" align="left" valign="middle" bgcolor="#3064C8" width="2%">&nbsp;</td>\n</tr>\n</tbody>\n</table>', 3, 'https://www.vagas.com.br/v1162872', 1430160563, 1433041200, 1, 'São Paulo, SP.'),
(172, '', NULL, 'Segmento ambiental', 'Analista Administrativo Jr', '<table id="Pr2C" style="width: 100%; height: 65%;" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor="#FFFFFF">\n<tbody>\n<tr>\n<td colspan="3" align="left" valign="top" width="80%">\n<table id="Pr2C" style="width: 100%; height: 65%;" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor="#FFFFFF">\n<tbody>\n<tr>\n<td colspan="3" align="left" valign="top" width="80%">\n<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta BPO &eacute; a divis&atilde;o especializada em Recrutamento e Sele&ccedil;&atilde;o, Tempor&aacute;rios e Outsourcing que vem desenvolvendo processos alinhados &agrave;s necessidades dos clientes e com a experi&ecirc;ncia de consultores altamente qualificados para atender organiza&ccedil;&otilde;es dos mais diversos segmentos.&nbsp;<br /><br />Estamos assessorando empresa nacional do segmento ambiental localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Analista Administrativo Jr</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Executar atividades de suporte aos processos de qualifica&ccedil;&atilde;o e certifica&ccedil;&atilde;o, principalmente atendimento ao cliente (telef&ocirc;nico, e-mail e pessoal).<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Superior completo ou cursando - Administra&ccedil;&atilde;o, Secretariado ou &aacute;reas afins;&nbsp;<br />&bull; Inform&aacute;tica: Conhecimentos no Pacote Office, principalmente Excel;&nbsp;<br />&bull; Idiomas: N&atilde;o se aplica;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: Atendimento ao cliente e rotinas administrativas.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Sal&aacute;rio: R$ 1.600,00;<br />&bull; Benef&iacute;cios: Assist&ecirc;ncia M&eacute;dica, Seguro de Vida, VT, Restaurante no local.;<br />&bull; Hor&aacute;rio: das 8h00 &agrave;s 18h00;<br />&bull; Local de trabalho: Vila Clementino (zona sul de S&atilde;o Paulo), pr&oacute;ximo ao metr&ocirc; Santa Cruz;<br />&bull; Modalidade contratual: Efetivo.</span></td>\n<td id="LtC" align="left" valign="middle" bgcolor="#3064C8" width="2%">&nbsp;</td>\n</tr>\n</tbody>\n</table>\n</td>\n<td id="LtC" align="left" valign="middle" bgcolor="#3064C8" width="2%">&nbsp;</td>\n</tr>\n</tbody>\n</table>', 1, 'https://www.vagas.com.br/v1176731', 1430160611, 1433041200, 1, 'São Paulo, SP.');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
