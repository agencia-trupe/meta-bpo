<?php

return array(

	'base' => array(

		'lang' => array(
			'trocar' => 'idioma/en',
			'outraVersao' => 'English Version',
			'outraVersaoReduzido' => 'english',
			'imagem' => 'lang-en.png',
		),

		'fechar' => 'Fechar',
		
		'nav' => array(

			'paginaInicial' => 'Página Inicial',
			'home' => 'Home',

			'empresa' => 'Empresa',
			'subempresa' => array(
				'quemsomos' => 'Quem Somos',
				'diferenciais' => 'Diferenciais',
			),

			'solucoes' => 'Soluções',

			'oportunidades' => 'Oportunidades',
			'oportunidadesExtendido' => 'Banco de Oportunidades',
			
			'cases' => 'Cases',
			'subcases' => array(
				'cases' => 'Cases',
				'depoimentos' => 'Depoimentos',
			),

			'contato' => 'Contato',
			'voltar' => 'voltar',
		),

		'barra' => array(
			'linha1' => 'CONHEÇA',
			'linha2' => 'NOSSOS',
			'linha3' => 'SERVIÇOS',
		),

		'campos' => array(
			'titulo' => 'titulo_pt',
			'slug' => 'slug_pt',
			'texto' => 'texto_pt',
			'autoria' => 'autoria_pt',
		),

		'footer' => array(
			'grupo' => 'A Meta BPO é uma divisão do',
			'direitos' => 'Grupo Meta RH &bull; Todos os direitos reservados',
		)
	),

	'empresa' => array(
		'titulo' => 'EMPRESA',
		'quemsomos' => 'QUEM SOMOS',
		'diferenciais' => 'DIFERENCIAIS',
		'responsavelpor' => 'Responsável por',
	),

	'oportunidades' => array(
		'titulo' => 'BANCO DE OPORTUNIDADES',
		'candidatese' => 'CANDIDATE-SE',
		'voltar' => 'voltar',
		'form' => array(
			'nome' => 'Nome',
			'email' => 'E-mail',
			'telefone' => 'Telefone',
			'curriculo' => 'Currículo [PDF, DOC]',
			'selecionarArquivo' => 'Selecionar arquivo',
			'observacoes' => 'Observações',
			'enviar' => 'ENVIAR &raquo;',
			'enviado' => 'Currículo enviado com sucesso!',
		),
	),

	'cases' => array(
		'titulo' => 'CASES',
		'depoimentos' => 'DEPOIMENTOS',
	),

	'contato' => array(
		'envieMensagem' => 'ENVIE-NOS UMA MENSAGEM',
		'entreContato' => 'ENTRE EM CONTATO',
		'form' => array(
			'enviado' => 'Sua mensagem foi enviada com sucesso!',
			'nome' => 'Nome',
			'email' => 'E-mail',
			'telefone' => 'Telefone',
			'mensagem' => 'Mensagem',
			'enviar' => 'ENVIAR &raquo;',
		)
	),

	'privacidade' => 'Política de Privacidade'
);
