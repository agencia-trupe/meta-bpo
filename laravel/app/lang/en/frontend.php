<?php

return array(
	'base' => array(
		'lang' => array(
			'trocar' => 'idioma/pt',
			'outraVersao' => 'Versão em Português',
			'outraVersaoReduzido' => 'português',
			'imagem' => 'lang-pt.png',
		),
	
		'fechar' => 'Close',
		
		'nav' => array(

			'paginaInicial' => 'Home',
			'home' => 'Home',

			'empresa' => 'Company',
			'subempresa' => array(
				'quemsomos' => 'About Us',
				'diferenciais' => 'Distinguishing aspects',
			),

			'solucoes' => 'Solutions',

			'oportunidades' => 'Opportunities Bank',
			'oportunidadesExtendido' => 'Opportunities Bank',
			
			'cases' => 'Cases',
			'subcases' => array(
				'cases' => 'Cases',
				'depoimentos' => 'Testimonials',
			),

			'contato' => 'Contact',
			'voltar' => 'back',
		),

		'barra' => array(
			'linha1' => 'SEE',
			'linha2' => 'OUR',
			'linha3' => 'SERVICES',
		),

		'campos' => array(
			'titulo' => 'titulo_en',
			'slug' => 'slug_en',
			'texto' => 'texto_en',
			'autoria' => 'autoria_en',
		),

		'footer' => array(
			'grupo' => 'Meta BPO is a division of',
			'direitos' => 'Grupo Meta RH &bull; All rights reserved',
		)
	),

	'empresa' => array(
		'titulo' => 'COMPANY',
		'quemsomos' => 'ABOUT US',
		'diferenciais' => 'DISTINGUISHING ASPECTS',
		'responsavelpor' => 'Responsible for',
	),

	'oportunidades' => array(
		'titulo' => 'OPPORTUNITIES BANK',
		'candidatese' => 'CANDIDATE-SE',
		'voltar' => 'back',
		'form' => array(
			'nome' => 'Nome',
			'email' => 'E-mail',
			'telefone' => 'Telefone',
			'curriculo' => 'Currículo [PDF, DOC]',
			'selecionarArquivo' => 'Selecionar arquivo',
			'observacoes' => 'Observações',
			'enviar' => 'SEND &raquo;',
			'enviado' => 'Currículo enviado com sucesso!',
		),
	),

	'cases' => array(
		'titulo' => 'CASES',
		'depoimentos' => 'TESTIMONIALS',
	),

	'contato' => array(
		'envieMensagem' => 'SEND US A MESSAGE',
		'entreContato' => 'CONTACT US',
		'form' => array(
			'enviado' => 'Sua mensagem foi enviada com sucesso!',
			'nome' => 'Name',
			'email' => 'E-mail',
			'telefone' => 'Telephone',
			'mensagem' => 'Message',
			'enviar' => 'SEND &raquo;',
		)
	),

	'privacidade' => 'Privacy Policy'
);
