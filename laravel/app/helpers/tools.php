<?php
class Tools {

    public static function formataTelefone($telefone = '')
    {
        if($telefone != ''){
            if(strpos($telefone, ' ') !== FALSE){
                $arr = explode(' ', $telefone);
                if(sizeof($arr) == 2) return "<small>".$arr[0]."</small> ".$arr[1];
                if(sizeof($arr) == 3) return "<small>".$arr[0].' '.$arr[1]."</small> ".$arr[2];
                if(sizeof($arr) == 4) return "<small>".$arr[0].' '.$arr[1]."</small> ".$arr[2].' '.$arr[3];
                return $telefone;
            }
        }else{
            return $telefone;
        }
    }

    public static function negritoUltimaPalavra($texto = ''){
        if($texto != '' && strpos($texto, ' ') !== FALSE){
            $arr = explode(' ', $texto);
            $arr[sizeof($arr) - 1] = "<strong>".end($arr)."</strong>";
            return implode(' ', $arr);
        }else{
            return $texto;
        }
    }

    public static function dataTimestamp($data = ''){
        if($data != ''){
            list($date,$time) = explode(' ', $data);
            return Tools::converteData($date);
        }else{
            return '';
        }
    }

    public static function formataData($data = '')
    {
        $meses = array(
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Março',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro'
        );

        if($data != ''){
            if(strpos($data, '-') !== FALSE){
                // Formato Americano -> Converter para BR
                list($ano, $mes, $dia) = explode('-', $data);
                return $dia.' de '.$meses[$mes].' / '.$ano;                
            }else{
                return $data;
            }
        }else{
            return $data;
        }
    }

    public static function converteData($data = '')
    {
        if($data != ''){
        	if(strpos($data, '-') !== FALSE){
        		// Formato Americano -> Converter para BR
        		list($ano, $mes, $dia) = explode('-', $data);
        		return $dia.'/'.$mes.'/'.$ano;                
        	}elseif(strpos($data, '/') !== FALSE){
        		// Formato BR -> Converter para Americano
        		list($dia, $mes, $ano) = explode('/', $data);
        		return $ano.'-'.$mes.'-'.$dia;
        	}
        }else{
        	return '';
        }
    }

    public static function exibeData($data = '')
    {

        $meses = array(
            '01' => 'jan',
            '02' => 'fev',
            '03' => 'mar',
            '04' => 'abr',
            '05' => 'mai',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'ago',
            '09' => 'set',
            '10' => 'out',
            '11' => 'nov',
            '12' => 'dez'
        );

        if ($data != '') {
            if(strpos($data, '-') !== FALSE){
                // Formato Americano -> Converter para BR
                list($ano, $mes, $dia) = explode('-', $data);
                return $dia.' '.$meses[$mes].' '.$ano;
            }elseif(strpos($data, '/') !== FALSE){
                // Formato BR -> Converter para Americano
                list($dia, $mes, $ano) = explode('/', $data);
                return $dia.' '.$meses[$mes].' '.$ano;
            }
        } else {
            return '';
        }
        
    }

    public static function validarData($data)
    {
        if($data != ''){
            if(strpos($data, '-') !== FALSE){
                // Formato Americano
                list($ano, $mes, $dia) = explode('-', $data);
                $ano_ok = (int) $ano > 0 && (int) $ano < 3000;
                $mes_ok = (int) $mes > 0 && (int) $mes <= 12;
                $dia_ok = (int) $dia > 0 && (int) $dia <= 31;
                return $ano_ok && $mes_ok && $dia_ok;
            }elseif(strpos($data, '/') !== FALSE){
                // Formato BR
                list($dia, $mes, $ano) = explode('/', $data);
                $ano_ok = (int) $ano > 0 && (int) $ano < 3000;
                $mes_ok = (int) $mes > 0 && (int) $mes <= 12;
                $dia_ok = (int) $dia > 0 && (int) $dia <= 31;
                return $ano_ok && $mes_ok && $dia_ok;                
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static function ip()
    {
        if(isset($_SERVER["REMOTE_ADDR"]))
            return $_SERVER["REMOTE_ADDR"];
        elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        elseif(isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];
        else
            return 'indeterminado';
    }

    public static function validarCpf($cpf)
    {
        $cpf = str_pad(preg_replace('/[^0-9]/i', '', $cpf), 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'){
            return false;
        }
        else{
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    public static function viewGMaps($str = '', $width = FALSE, $height = FALSE){
        //$str = stripslashes(htmlspecialchars_decode($str));

        if($width !== FALSE)
            $str = preg_replace('~width=\"(\d+)\"~', 'width="'.$width.'"', $str);
        if($height !== FALSE)
            $str = preg_replace('~height=\"(\d+)\"~', 'height="'.$height.'"', $str);

        return preg_replace("~<br \/>(.*)~", "", $str);
    }
}
