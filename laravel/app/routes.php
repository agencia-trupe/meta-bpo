<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('/home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('empresa', array('as' => 'quemsomos', 'uses' => 'QuemsomosController@index'));
Route::get('empresa/quemsomos', array('as' => 'quemsomos', 'uses' => 'QuemsomosController@index'));
Route::get('politica-de-privacidade', array('as' => 'privacidade', 'uses' => 'PrivacidadeController@index'));
Route::get('empresa/diferenciais', array('as' => 'diferenciais', 'uses' => 'DiferenciaisController@index'));
Route::get('solucoes/{slug?}', array('as' => 'solucoes', 'uses' => 'SolucoesController@index'));
// Route::get('oportunidades', array('as' => 'oportunidades', 'uses' => 'OportunidadesController@index'));
// Route::get('oportunidades/detalhe/{slug?}', array('as' => 'oportunidades', 'uses' => 'OportunidadesController@detalhe'));
// Route::post('oportunidades/enviar', array('as' => 'oportunidades.enviar', 'uses' => 'OportunidadesController@enviar'));
Route::get('cases', array('as' => 'cases', 'uses' => 'CasesController@index'));
Route::get('cases/cases', array('as' => 'cases', 'uses' => 'CasesController@index'));
Route::get('cases/depoimentos', array('as' => 'cases.depoimentos', 'uses' => 'DepoimentosController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato/enviar', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));

/* LOCALIZAÇÃO */
Route::get('idioma/{sigla_idioma?}', function($sigla_idioma){
	if($sigla_idioma == 'pt' || $sigla_idioma == 'en')
		Session::put('locale', $sigla_idioma);

	return Redirect::back();
});

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
	Route::get('downloadCV/{id}', function($id){

		$candidato = Candidato::find($id);

		return Response::download(base_path('curriculos/'.$candidato->curriculo));
	});

	Route::post('candidatos/buscarNome', array('as' => 'painel.buscacandidato.nome', 'uses' => 'Painel\CandidatosController@buscarNome'));
	Route::post('candidatos/buscarData', array('as' => 'painel.buscacandidato.data', 'uses' => 'Painel\CandidatosController@buscarData'));

    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banners', 'Painel\BannersController');
	Route::resource('quemsomos', 'Painel\QuemSomosController');
	Route::resource('politica-de-privacidade', 'Painel\PrivacidadeController');
	Route::resource('equipe', 'Painel\EquipeController');
	Route::resource('diferenciais', 'Painel\DiferenciaisController');
	Route::resource('cursos', 'Painel\CursosController');
	// Route::resource('oportunidades', 'Painel\OportunidadesController');
	// Route::resource('oportunidadesarquivadas', 'Painel\OportunidadesArquivadasController');
	Route::resource('candidatos', 'Painel\CandidatosController');
	Route::resource('cases', 'Painel\CasesController');
	Route::resource('depoimentos', 'Painel\DepoimentosController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('solucoes', 'Painel\SolucoesController');
//NOVASROTASDOPAINEL//
});
