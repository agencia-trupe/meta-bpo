@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Equipe @if(sizeof(Equipe::all()) < $limiteInsercao) <a href='{{ URL::route('painel.equipe.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Membro da Equipe</a> @endif
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='equipe'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Nome</th>
				<th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm btn-small'>mover</a></td>
				<td>
                    {{ $registro->titulo_pt }}
				</td>
				<td>
                    Português : <br> {{ Str::words(strip_tags($registro->texto_pt), 15) }} <hr>
				    Inglês: <br> {{ Str::words(strip_tags($registro->texto_en), 15) }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.equipe.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>					
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop