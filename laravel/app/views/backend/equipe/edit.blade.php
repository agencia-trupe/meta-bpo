@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Membro da Equipe
        </h2>  

		{{ Form::open( array('route' => array('painel.equipe.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítuloPT">Nome</label>
					<input type="text" class="form-control" id="inputTítuloPT" name="titulo_pt" value="{{$registro->titulo_pt}}" required>
				</div>

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/equipe/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				
				<div class="form-group">
					<label for="inputDivisão">Divisão</label>					 
					<select	name="divisao" class="form-control" id="inputDivisão" required>
						<option value="">Selecione</option>
						<option value="executive_search" @if($registro->divisao == 'executive_search') selected @endif>Executive Search</option>
						<option value="desenvolvimento_organizacional" @if($registro->divisao == 'desenvolvimento_organizacional') selected @endif>Desenvolvimento Organizacional</option>
					</select>
				</div>
				
				<div class="form-group">
					<label for="inputTextoPT">Texto em Português</label>
					<textarea name="texto_pt" class="form-control" id="inputTextoPT" >{{$registro->texto_pt }}</textarea>
				</div>
				<div class="form-group">
					<label for="inputTextoEN">Texto em Inglês</label>
					<textarea name="texto_en" class="form-control" id="inputTextoEN" >{{$registro->texto_en }}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.equipe.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop