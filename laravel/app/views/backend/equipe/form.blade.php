@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Membro da Equipe
        </h2>  

		<form action="{{URL::route('painel.equipe.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítuloPT">Nome</label>
					<input type="text" class="form-control" id="inputTítuloPT" name="titulo_pt" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo_pt'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>
				
				<div class="form-group">
					<label for="inputDivisão">Divisão</label>					 
					<select	name="divisao" class="form-control" id="inputDivisão" required>
						<option value="">Selecione</option>
						<option value="executive_search">Executive Search</option>
						<option value="desenvolvimento_organizacional">Desenvolvimento Organizacional</option>
					</select>
				</div>
				
				<div class="form-group">
					<label for="inputTextoPT">Texto em Português</label>
					<textarea name="texto_pt" class="form-control" id="inputTextoPT" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_pt'] }} @endif</textarea>
				</div>	
				<div class="form-group">
					<label for="inputTextoEN">Texto em Inglês</label>
					<textarea name="texto_en" class="form-control" id="inputTextoEN" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_en'] }} @endif</textarea>
				</div>	

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.equipe.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop