@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Contato 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Telefone</th>
				<th>Social Media</th>
				<th>Endereço</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->telefone }}</td>
				<td style="text-align:center;">
                    @if($registro->facebook) <a href="{{ $registro->facebook }}" class="btn btn-sm btn-info" target="_blank" style="margin:3px auto;">Facebook</a><br> @endif
				    @if($registro->twitter) <a href="{{ $registro->twitter }}" class="btn btn-sm btn-info" target="_blank" style="margin:3px auto;">Twitter</a><br> @endif
				    @if($registro->linkedin) <a href="{{ $registro->linkedin }}" class="btn btn-sm btn-info" target="_blank" style="margin:3px auto;">LinkedIn</a> @endif
                </td>
				<td>{{ Str::words(strip_tags($registro->endereco), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop