@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Informações de Contato
        </h2>  

		<form action="{{URL::route('painel.contato.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['telefone'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['facebook'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputTwitter">Twitter</label>
					<input type="text" class="form-control" id="inputTwitter" name="twitter" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['twitter'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputLinkedIn">LinkedIn</label>
					<input type="text" class="form-control" id="inputLinkedIn" name="linkedin" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['linkedin'] }}" @endif >
				</div>
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<textarea name="endereco" class="form-control" id="inputEndereço" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['endereco'] }} @endif</textarea>
				</div>	
				<div class="form-group">
					<label for="inputGoogle Maps">Google Maps</label>
					<input type="text" class="form-control" id="inputGoogle Maps" name="google_maps" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['google_maps'] }}" @endif >
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop