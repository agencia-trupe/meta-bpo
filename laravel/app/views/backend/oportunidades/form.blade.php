@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Oportunidade
        </h2>  

		<form action="{{URL::route('painel.oportunidades.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputIdioma">Idioma</label>					 
					<select	name="idioma" class="form-control" id="inputIdioma" required>
						<option value="">Selecione</option>
						<option value="pt">Português</option>
						<option value="en">Inglês</option>
					</select>
				</div>

				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['empresa'] }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título da Oportunidade</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputNPosicoes">Número de Posições</label>
					<input type="text" class="form-control" id="inputNPosicoes" name="n_posicoes" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['n_posicoes'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputCidade">Cidade</label>
					<input type="text" class="form-control" id="inputCidade" name="cidade" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['cidade'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputEstado">Estado</label>
					<input type="text" maxlength="2" class="form-control" id="inputEstado" name="estado" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['estado'] }}" @endif >					
				</div>

				<div class="form-group">
					<label for="inputDataCadastro">Data de Cadastro</label>
					<input type="text" class="form-control datepicker" id="inputDataCadastro" name="data_cadastro" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['data_cadastro'] }}" @else value="{{date('d/m/Y')}}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto'] }} @endif</textarea>
				</div>

				<hr>
				<div class="form-group">
					<div class="alert alert-info">Caso seja informada uma Data de Expiração, a oportunidade será arquivada automaticamente no dia.</div>
					<label for="inputDataExpiração">Data de Expiração <small>(opcional)</small></label>
					<input type="text" class="form-control datepicker" id="inputDataExpiração" name="data_expiracao" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['data_expiracao'] }}" @endif >
				</div>
				<hr>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.oportunidades.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop