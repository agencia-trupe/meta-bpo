@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Oportunidades <span class="label label-primary">Ativas</span> <a href='{{ URL::route('painel.oportunidades.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Oportunidade</a>
    </h2>

    <hr>

    <div class="btn-group">
        <a href="{{URL::route('painel.oportunidades.index', array('idioma' => 'pt'))}}" class="btn btn-default @if(isset($idioma) && $idioma == 'pt') btn-info @endif">Oportunidades em Português</a>
        <a href="{{URL::route('painel.oportunidades.index', array('idioma' => 'en'))}}" class="btn btn-default @if(isset($idioma) && $idioma == 'en') btn-info @endif">Oportunidades em Inglês</a>
    </div>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Empresa</th>
				<th>Título</th>
				<th>Local</th>
				<th>Data de Cadastro</th>
				<th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->empresa }}</td>
				<td>{{ $registro->titulo }}</td>
				<td>{{ $registro->cidade .' - '. $registro->estado }}</td>
				<td>{{ Tools::converteData($registro->data_cadastro) }}</td>
				<td class="crud-actions" style="width:145px">
                    <a href='{{ URL::route('painel.oportunidades.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.oportunidades.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-warning btn-sm btn-arquivar'>arquivar</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop