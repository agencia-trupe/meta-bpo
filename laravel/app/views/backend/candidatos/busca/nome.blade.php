@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Candidatos <small>Resultado da busca por Nome : {{$termo}}</small>
    </h2>
    
    <hr>
    <form action="painel/candidatos/buscarNome" method="post">
        <div class="row">
            <div class="col-lg-4">
                <label>Pesquisar por Nome</label>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Pesquisar por Nome" required name="nome" @if(isset($termo) && $termo) {{"value='$termo'"}} @endif>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </div>
        </div>
    </form>
    <hr>
    <form action="painel/candidatos/buscarData" method="post">
        <label>Pesquisar por Data</label>
        <div class="row">
            <div class="col-lg-2">
                <input type="text" class="form-control datepicker" placeholder="Data Inicial" required name="dt_inicial" @if(isset($termo_inicial) && $termo_inicial) {{"value='$termo_inicial'"}} @endif>
            </div>
            <div class="col-lg-2">
                <input type="text" class="form-control datepicker" placeholder="Data Final (opcional)" name="dt_final" @if(isset($termo_final) && $termo_final) {{"value='$termo_final'"}} @endif>
            </div>
            <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>            
        </div>
    </form>
    <hr>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Vaga de Interesse</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Observações</th>
                <th>Currículo</th>
                <th>Data de Envio</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @if(sizeof($registros))
                @foreach ($registros as $registro)

                    <tr class="tr-row" id="row_{{ $registro->id }}">
                        <td>
                            {{$registro->oportunidade->empresa}}
                            <br>
                            {{$registro->oportunidade->titulo}}
                        </td>
                        <td>{{$registro->nome}}</td>
                        <td><a href="mailto:{{$registro->email}}" title="Enviar E-mail" class="btn btn-sm">{{$registro->email}}</a></td>
                        <td>{{$registro->telefone}}</td>
                        <td>{{$registro->observacoes}}</td>
                        <td><a href="painel/downloadCV/{{$registro->id}}" title="Fazer Download" class="btn btn-default btn-sm">download</a></td>
                        <td>{{Tools::dataTimestamp($registro->created_at)}}</td>
                        <td class="crud-actions">
                            {{ Form::open(array('route' => array('painel.candidatos.destroy', $registro->id), 'method' => 'delete')) }}
        						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
        					{{ Form::close() }}                    
                        </td>
                    </tr>

                @endforeach
            @else
                <tr>
                    <td colspan="8">Nenhum Registro Encontrado</td>
                </tr>
            @endif
        </tbody>

    </table>
</div>

@stop