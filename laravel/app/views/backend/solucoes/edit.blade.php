@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.solucoes.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítuloPT">Título em Português</label>
					<input type="text" class="form-control" id="inputTítuloPT" name="titulo_pt" value="{{$registro->titulo_pt}}" disabled>
				</div>

				<div class="form-group">
					<label for="inputTítuloEN">Título em Inglês</label>
					<input type="text" class="form-control" id="inputTítuloEN" name="titulo_en" value="{{$registro->titulo_en}}" disabled>
				</div>

				<div class="form-group">
					<label for="inputTextoPT">Texto em Português</label>
					<textarea name="texto_pt" class="form-control" id="inputTextoPT" >{{$registro->texto_pt }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoEN">Texto em Inglês</label>
					<textarea name="texto_en" class="form-control" id="inputTextoEN" >{{$registro->texto_en }}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.solucoes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
