@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Cursos <a href='{{ URL::route('painel.cursos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Curso</a>
    </h2>

    <a href="{{URL::route('painel.solucoesdev.index')}}" title="Voltar" class="btn btn-default">&larr; Voltar Para Soluções - Desenvolvimento Organizacional</a>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='cursos'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>				
				<th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm btn-small'>mover</a></td>
				<td>{{ $registro->titulo_pt }}</td>
				<td>
                    Português : <br> {{ Str::words(strip_tags($registro->texto_pt), 15) }}
                    <hr>
				    Inglês : <br> {{ Str::words(strip_tags($registro->texto_en), 15) }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.cursos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.cursos.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop