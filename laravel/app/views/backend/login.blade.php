@section('conteudo')

<div class="login">  	
			
	<form action="{{ URL::route('painel.login') }}" method="post">

  		<h3>Meta BPO <small>Painel Administrativo</small></h3>

  		@if(Session::has('login_errors'))
			<div class="alert alert-danger">Usuário ou Senha incorretos</div>
		@endif
		
		<div class="input-group">
			<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
			<input type="text" class="form-control" placeholder="Usuário" required name="username" autofocus>
		</div>

		<div class="input-group">
			<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
			<input type="password" class="form-control" placeholder="Senha" required name="password">
		</div>
		
		<div class="submit-placeholder">
			<label>
				<input type="checkbox" value="1" name="lembrar"> Lembrar de mim
			</label>
   			<input type="submit" class="btn btn-primary" value="Login">
		</div>

	</form>

</div>

@stop
