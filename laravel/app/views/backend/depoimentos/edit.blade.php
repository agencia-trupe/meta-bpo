@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Depoimento
        </h2>  

		{{ Form::open( array('route' => array('painel.depoimentos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" value="{{$registro->empresa}}" required>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título em Português</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo_pt" value="{{$registro->titulo_pt}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título em Inglês</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo_en" value="{{$registro->titulo_en}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto em Português</label>
					<textarea name="texto_pt" class="form-control" id="inputTexto" >{{$registro->texto_pt }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto em Inglês</label>
					<textarea name="texto_en" class="form-control" id="inputTexto" >{{$registro->texto_en }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputAutoria">Autoria em Português</label>
					<input type="text" class="form-control" id="inputAutoria" name="autoria_pt" value="{{$registro->autoria_pt}}" >
				</div>
				
				<div class="form-group">
					<label for="inputAutoria">Autoria em Inglês</label>
					<input type="text" class="form-control" id="inputAutoria" name="autoria_en" value="{{$registro->autoria_en}}" >
				</div>
				
				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/depoimentos/{{$registro->imagem}}"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.depoimentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop