@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Depoimentos <a href='{{ URL::route('painel.depoimentos.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Depoimento</a>
    </h2>

    <table class='table table-striped table-bordered table-hover' data-tabela='depoimentos'>

        <thead>
            <tr>
                <th>Empresa</th>
                <th>Título</th>
				<th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->empresa }}</td>
                <td>{{ $registro->titulo_pt }}</td>
				<td>
                    Português : <br>{{ Str::words(strip_tags($registro->texto_pt), 15) }}
                    <hr>
				    Inglês : <br>{{ Str::words(strip_tags($registro->texto_en), 15) }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.depoimentos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.depoimentos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop