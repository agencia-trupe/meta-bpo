<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Meta BPO - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/jquery-ui/themes/base/jquery-ui',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Meta BPO</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li @if(str_is('painel.banner*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.banners.index')}}" title="Banners">Banners</a>
						</li>

						<li class="dropdown @if(str_is('painel.quemsomos*', Route::currentRouteName()) || str_is('painel.equipe*', Route::currentRouteName()) || str_is('painel.diferenciais*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Empresa <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.quemsomos.index')}}" title="Quem Somos">Quem Somos</a></li>
								<li><a href="{{URL::route('painel.diferenciais.index')}}" title="Diferenciais">Diferenciais</a></li>								
							</ul>
						</li>

						<li @if(str_is('painel.solucoes*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.solucoes.index')}}" title="Soluções">Soluções</a>
						</li>

						<li class="dropdown @if(str_is('painel.cases*', Route::currentRouteName()) || str_is('painel.depoimentos*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Cases <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.cases.index')}}" title="Cases">Cases</a></li>
								<li><a href="{{URL::route('painel.depoimentos.index')}}" title="Depoimentos">Depoimentos</a></li>								
							</ul>
						</li>

						<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a>
						</li>

						<li @if(str_is('painel.politica-de-privacidade*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.politica-de-privacidade.index')}}" title="Política de Privacidade">Política de Privacidade</a>
						</li>

						<li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>
		
		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/ckeditor/config',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/ckeditor/config',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
