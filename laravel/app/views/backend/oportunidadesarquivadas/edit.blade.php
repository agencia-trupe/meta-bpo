@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Oportunidade
        </h2>  

		{{ Form::open( array('route' => array('painel.oportunidadesarquivadas.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inputIdioma">Idioma</label>					 
					<select	name="idioma" class="form-control" id="inputIdioma" required>
						<option value="">Selecione</option>
						<option value="pt" @if($registro->idioma == 'pt') selected @endif>Português</option>
						<option value="en" @if($registro->idioma == 'en') selected @endif>Inglês</option>
					</select>
				</div>

				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="empresa" value="{{$registro->empresa}}" required>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título da Oportunidade</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="form-group">
					<label for="inputNPosições">Número de Posições</label>
					<input type="text" class="form-control" id="inputNPosições" name="n_posicoes" value="{{$registro->n_posicoes}}" >
				</div>

				<div class="form-group">
					<label for="inputCidade">Cidade</label>
					<input type="text" class="form-control" id="inputCidade" name="cidade" value="{{$registro->cidade}}" >
				</div>

				<div class="form-group">
					<label for="inputEstado">Estado</label>
					<input type="text" maxlength="2" class="form-control" id="inputEstado" name="estado" value="{{$registro->estado}}" >
				</div>

				<div class="form-group">
					<label for="inputData de Cadastro">Data de Cadastro</label>
					<input type="text" class="form-control datepicker" id="inputData de Cadastro" name="data_cadastro" value="{{ Tools::converteData($registro->data_cadastro) }}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<hr>
				<div class="form-group">
					<div class="alert alert-info">Caso seja informada uma Data de Expiração, a oportunidade será arquivada automaticamente no dia.</div>
					<label for="inputDataExpiração">Data de Expiração <small>(opcional)</small></label>
					<input type="text" class="form-control datepicker" id="inputDataExpiração" name="data_expiracao" value="{{ Tools::converteData($registro->data_expiracao) }}" >
				</div>
				<hr>

				<button type="submit" title="Alterar" name="submitbtn" value="alterar" class="btn btn-success">Alterar e manter Desativada</button>
				<button type="submit" title="Alterar" name="submitbtn" value="reativar" class="btn btn-warning">Alterar e Reativar a Oportunidade</button>

				<a href="{{URL::route('painel.oportunidadesarquivadas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop

