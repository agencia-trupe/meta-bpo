@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Política de Privacidade
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
                <th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>Política de Privacidade</td>
                <td>
                    Português : <br>{{ Str::words(strip_tags($registro->texto_pt), 30) }}
                    <hr>
				    Inglês : <br>{{ Str::words(strip_tags($registro->texto_en), 30) }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.politica-de-privacidade.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop