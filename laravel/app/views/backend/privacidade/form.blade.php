@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.politica-de-privacidade.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTextoPT">TextoPT</label>
					<textarea name="texto_pt" class="form-control" id="inputTextoPT" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_pt'] }} @endif</textarea>
				</div>	
				<div class="form-group">
					<label for="inputTextoEN">TextoEN</label>
					<textarea name="texto_en" class="form-control" id="inputTextoEN" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_en'] }} @endif</textarea>
				</div>	

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.politica-de-privacidade.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop