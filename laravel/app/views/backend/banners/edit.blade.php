@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Banner
        </h2>  

		{{ Form::open( array('route' => array('painel.banners.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inputTexto">Texto em Português <small>(Use &lt;br&gt; para quebra de linhas)</small></label>
					<input type="text" name="texto_pt" class="form-control" id="inputTexto" value="{{$registro->texto_pt}}">
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto em Inglês <small>(Use &lt;br&gt; para quebra de linhas)</small></label>
					<input type="text" name="texto_en" class="form-control" id="inputTexto" value="{{$registro->texto_en}}">
				</div>	

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/banners/{{$registro->imagem}}" style="max-width:100%;"><br>
					@endif
					<label for="inputImagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" value="{{$registro->link}}" required>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.banners.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
