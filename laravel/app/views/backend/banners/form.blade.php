@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Banner
        </h2>  

		<form action="{{URL::route('painel.banners.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTexto">Texto em Português <small>(Use &lt;br&gt; para quebra de linhas)</small></label>
					<input type="text" name="texto_pt" class="form-control" id="inputTexto" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['texto_pt'] }}" @endif>
				</div>	
				
				<div class="form-group">
					<label for="inputTexto">Texto em Inglês <small>(Use &lt;br&gt; para quebra de linhas)</small></label>
					<input type="text" name="texto_en" class="form-control" id="inputTexto" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['texto_en'] }}" @endif>
				</div>	

				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['link'] }}" @endif required>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.banners.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
