@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Banners <a href='{{ URL::route('painel.banners.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Banner</a>
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='novosite_banners'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Imagem</th>
                <th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm btn-small'>mover</a></td>
				<td><img src='assets/images/banners/{{ $registro->imagem }}' style='max-width:150px'></td>
                <td>
                    Português : <br>{{ Str::words(strip_tags($registro->texto_pt), 30) }}
                    <hr>
                    Inglês : <br>{{ Str::words(strip_tags($registro->texto_en), 30) }}
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.banners.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.banners.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop
