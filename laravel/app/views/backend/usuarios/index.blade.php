@section('conteudo')

    <div class="container">

    	@if(Session::has('sucesso'))
    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
        @endif

    	@if($errors->any())
    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    	@endif

      	<h2>
        	Usuários <a href="{{ URL::route('painel.usuarios.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Usuário</a>
        </h2>

      	<table class="table table-striped table-bordered table-hover ">

      		<thead>
        		<tr>
          			<th>Usuário</th>
          			<th>E-mail</th>
          			<th><span class="glyphicon glyphicon-cog"></span></th>
        		</tr>
      		</thead>

      		<tbody>
        	@foreach ($usuarios as $usuario)

            	<tr class="tr-row">
              		<td>
                        {{ $usuario->username }}
                    </td>
              		<td>
                        {{ $usuario->email }}
                    </td>
              		<td class="crud-actions">
                		<a href="{{ URL::route('painel.usuarios.edit', $usuario->id ) }}" class="btn btn-primary btn-sm">editar</a>
                        
                        @if(Auth::user()->id != $usuario->id)
                    	   {{ Form::open(array('route' => array('painel.usuarios.destroy', $usuario->id), 'method' => 'delete')) }}
                                <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                           {{ Form::close() }}
                       @endif
              		</td>
            	</tr>

        	@endforeach
      		</tbody>

    	</table>
    </div>

@stop
