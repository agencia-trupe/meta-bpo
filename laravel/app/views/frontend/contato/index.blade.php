@section('conteudo')

	{{Tools::viewGMaps($contato->google_maps, '100%', '280px')}}

	<div class="main centro">
		
		<div class="form">

			<h2>
				<span>{{Tools::negritoUltimaPalavra(Lang::get('frontend.contato.envieMensagem'))}}</span>
			</h2>

			@if(Session::has('enviado'))

				<div class="enviado">{{Lang::get('frontend.contato.form.enviado')}}</div>

			@else

				<form action="{{URL::route('contato.enviar')}}" method="post">
					<input type="text" name="nome" id="input-nome" placeholder="{{Lang::get('frontend.contato.form.nome')}}" required @if(Session::has('form-nome'))value="{{Session::get('form-nome')}}"@endif>
					<input type="email" name="email" id="input-email" placeholder="{{Lang::get('frontend.contato.form.email')}}" required @if(Session::has('form-email'))value="{{Session::get('form-email')}}"@endif>
					<input type="text" name="telefone" id="input-telefone" placeholder="{{Lang::get('frontend.contato.form.telefone')}}" @if(Session::has('form-telefone'))value="{{Session::get('form-telefone')}}"@endif>
					<textarea name="mensagem" id="input-mensagem" placeholder="{{Lang::get('frontend.contato.form.mensagem')}}">@if(Session::has('form-mensagem')){{Session::get('form-mensagem')}}@endif</textarea>
					<div class="enviar">
						<button type="submit"><span>{{Lang::get('frontend.oportunidades.form.enviar')}}</span></button>
					</div>
				</form>

			@endif
		</div>

		<div class="info">
			<h2>
				<span>{{Tools::negritoUltimaPalavra(Lang::get('frontend.contato.entreContato'))}}</span>
			</h2>
			<div class="telefone">
				{{Tools::formataTelefone($contato->telefone)}}
			</div>
			<div class="endereco">
				{{$contato->endereco}}
			</div>
			<div class="assinatura">
				<p>{{Lang::get('frontend.base.footer.grupo')}}</p>
				<a href="http://www.grupometarh.com.br" target="_blank"><img src="assets/images/layout/contato-grupometarh.png" alt="Grupo Meta RH"></a>
			</div>
		</div>

	</div>

	@if($errors->any())
		<script defer>
			$('document').ready( function(){
				alert('{{$errors->first()}}');
			});
		</script>
	@endif

@stop
