@section('conteudo')
	
	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			<span>{{ Lang::get('frontend.empresa.titulo') }} &raquo;</span> {{ Lang::get('frontend.empresa.quemsomos') }}
		</h1>
	</dix>

	<div class="conteudo">
		<div class="centro">
			<div class="imagem">
				<img src="assets/images/layout/quemsomos-foto.jpg" alt="{{ Lang::get('frontend.empresa.quemsomos') }}">
			</div>

			<div class="texto cke">
				{{$texto->{Lang::get('frontend.base.campos.texto')} }}

				<a href="empresa/diferenciais" class="link-diferenciais" title="{{Lang::get('frontend.empresa.diferenciais')}}">
					<span>{{Lang::get('frontend.empresa.diferenciais')}} &raquo;</span>
				</a>
			</div>
		</div>
	</div>

@stop
