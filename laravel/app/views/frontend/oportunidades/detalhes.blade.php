@section('conteudo')

	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			{{ Lang::get('frontend.oportunidades.titulo') }}
		</h1>
	</dix>

	<div class="centro detalhes">

		<section>
			<div class="ofertante">{{$detalhes->empresa}}</div>
			<div class="olho">
				<span class="titulo">{{$detalhes->titulo}}</span>
				<span class="posicao">{{$detalhes->n_posicoes}}</span>
				<span class="local">{{$detalhes->cidade}}, {{$detalhes->estado}}</span>
				<span class="data">{{ Tools::formataData($detalhes->data_cadastro) }}</span>
			</div>
			<div class="texto cke">
				{{$detalhes->texto}}
			</div>
		</section>

		<aside>

			<h2>
				<span>{{Lang::get('frontend.oportunidades.candidatese')}}</span>
			</h2>

			@if(Session::has('enviado'))

				<div class="enviado">
					{{Lang::get('frontend.oportunidades.form.enviado')}}
				</div>

			@else

				<form action="{{URL::route('oportunidades.enviar')}}" method="post" id="form-candidato" enctype="multipart/form-data" novalidate>
					<input type="text" name="nome" id="input-nome" placeholder="{{Lang::get('frontend.oportunidades.form.nome')}}" required @if(Session::has('form-nome'))value="{{Session::get('form-nome')}}"@endif>
					<input type="email" name="email" id="input-email" placeholder="{{Lang::get('frontend.oportunidades.form.email')}}" required @if(Session::has('form-email'))value="{{Session::get('form-email')}}"@endif>
					<input type="text" name="telefone" id="input-telefone" placeholder="{{Lang::get('frontend.oportunidades.form.telefone')}}" @if(Session::has('form-telefone'))value="{{Session::get('form-telefone')}}"@endif>
					<input type="hidden" name="oportunidades_id" value="{{$detalhes->id}}">
					<label>
						<div class="fakebutton">
							{{Lang::get('frontend.oportunidades.form.selecionarArquivo')}}
						</div>
						<span>{{Lang::get('frontend.oportunidades.form.curriculo')}}</span>
						<input type="file" name="curriculo" required id="input-curriculo">
					</label>
					<textarea name="observacoes" id="input-observacoes" placeholder="{{Lang::get('frontend.oportunidades.form.observacoes')}}">@if(Session::has('form-observacoes')){{Session::get('form-observacoes')}}@endif</textarea>
					<div class="enviar">
						<button type="submit"><span>{{Lang::get('frontend.oportunidades.form.enviar')}}</span></button>
					</div>
				</form>

			@endif

			
		</aside>

		<div class="voltar">
			@if(Session::has('enviado'))
				<a href="oportunidades" title="{{Lang::get('frontend.oportunidades.voltar')}}">&laquo; {{Lang::get('frontend.oportunidades.voltar')}}</a>
			@else
				<a href="{{URL::previous()}}" title="{{Lang::get('frontend.oportunidades.voltar')}}">&laquo; {{Lang::get('frontend.oportunidades.voltar')}}</a>
			@endif
		</div>
	</div>

	@if($errors->any())
		<script defer>
			$('document').ready( function(){
				alert('{{$errors->first()}}');
			});
		</script>
	@endif

@stop