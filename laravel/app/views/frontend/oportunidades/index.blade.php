@section('conteudo')

	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			{{ Lang::get('frontend.oportunidades.titulo') }}
		</h1>
	</dix>

	<div class="centro">
		@if($oportunidades)
			@foreach($oportunidades as $op)
				<a href="oportunidades/detalhe/{{$op->slug}}" title="{{$op->{Lang::get('frontend.base.campos.titulo')} }}" class="oportunidade">
					<div class="ofertante">
						{{$op->empresa}}
					</div>
					<div class="olho">
						<div class="titulo">
							{{$op->titulo}}	
						</div>
						<div class="detalhes">
							<span class="posicao">{{$op->n_posicoes}}</span>
							<span class="local">{{$op->cidade}}, {{$op->estado}}</span>
						</div>
						<div class="data">
							{{ Tools::formataData($op->data_cadastro) }}
						</div>
					</div>
				</a>
			@endforeach
			
			<div class="separador"></div>
			{{$oportunidades->links()}}

		@endif
	</div>

@stop
