@section('conteudo')

	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			<span>{{ Lang::get('frontend.cases.titulo') }} &raquo;</span> {{ Lang::get('frontend.cases.titulo') }}
		</h1>
	</dix>
	<div class="main">
		<div class="listaCases">
			@if($cases)
				<?php $contador = 0 ?>
				<span>
				@foreach($cases as $k => $cases)

					<?php $contador++ ?>

					<a href="#" title="{{$cases->empresa}}" data-indice="{{$contador}}" data-imagem="{{$cases->imagem}}" data-titulo="{{$cases->{Lang::get('frontend.base.campos.titulo')} }}" data-texto="{{htmlentities($cases->{Lang::get('frontend.base.campos.texto')}) }}">
						<img src="assets/images/cases/thumbs/{{$cases->imagem}}" alt="{{$cases->empresa}}">
					</a>

					<?php $abriu = true; ?>

					@if($contador == 5)
						</span>
						<?php $contador = 0 ?>

						<div class="detalhes">
							<div class="centro">
								<div class="setaUp"></div>
								<div href="#" class="fechar" title="{{Lang::get('frontend.base.fechar')}}"></div>
								<div class="imagem">

								</div>
								<div class="texto">
									<h2></h2>
									<div class="cke"></div>
								</div>
							</div>
						</div>
						<span>
						<?php $abriu = false; ?>
					@endif

				@endforeach

				@if($abriu)
					</span>
					<div class="detalhes">
						<div class="centro">
							<div class="setaUp"></div>
							<div href="#" class="fechar" title="{{Lang::get('frontend.base.fechar')}}"></div>
							<div class="imagem">

							</div>
							<div class="texto">
								<h2></h2>
								<div class="cke"></div>
							</div>
						</div>
					</div>
					<span>
				@endif
				</span>
			@endif
		</div>
	</div>
@stop
