@section('conteudo')

	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			<span>{{ Lang::get('frontend.cases.titulo') }} &raquo;</span> {{ Lang::get('frontend.cases.depoimentos') }}
		</h1>
	</dix>
	
	<div class="centro">
		@if(sizeof($depoimentos))
			@foreach($depoimentos as $k => $dep)
				<div class="dep">
					<div class="imagem">
						<img src="assets/images/depoimentos/{{$dep->imagem}}" alt="{{$dep->empresa }}">
					</div>
					<div class="texto">
						<h2>
							{{$dep->{Lang::get('frontend.base.campos.titulo')} }}
						</h2>
						<div class="cke">
							{{$dep->{Lang::get('frontend.base.campos.texto')} }}
						</div>
						<div class="autor">
							{{$dep->{Lang::get('frontend.base.campos.autoria')} }}
						</div>
					</div>
				</div>
			@endforeach
		@endif
	</div>
	
@stop
