<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">

    <meta name="keywords" content="" />

	<title>Meta BPO</title>
	<meta name="description" content="">
	<meta property="og:title" content="Meta BPO"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Meta BPO"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="A Meta BPO é a divisão do Grupo Meta RH especializada em prover soluções em Recrutamento & Seleção, Gestão de Profissionais Temporários e Outsourcing, desenvolvendo processos alinhados às necessidades dos clientes, com a solidez de mais de 30 anos no mercado de Recursos Humanos.">
    <meta property="og:image" content="http://metabpo.com.br/assets/images/layout/logo-fb.png">
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr'))?>
	@endif

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-42865093-1', 'auto');
  ga('send', 'pageview');
 
</script>
</head>
<body>

	<header>
		<div id="aux-nav">
			<div class="centro">
				<div class="telefone">
					@if($contato->telefone)
						{{ Tools::formataTelefone($contato->telefone) }}
					@endif
				</div>
				<ul>
					@if($contato->facebook)
						<li><a href="{{ $contato->facebook }}" class="fb-link" target="_blank" title="Facebook">Facebook</a></li>
					@endif
					@if($contato->twitter)
						<li><a href="{{ $contato->twitter }}" class="tw-link" target="_blank" title="Twitter">Twitter</a></li>
					@endif
					@if($contato->linkedin)
						<li><a href="{{ $contato->linkedin }}" class="lk-link" target="_blank" title="Linkedin">Linkedin</a></li>
					@endif
					<li>
						<a href="{{ Lang::get('frontend.base.lang.trocar') }}" title="{{ Lang::get('frontend.base.lang.outraVersao') }}">
							<img src="assets/images/layout/{{ Lang::get('frontend.base.lang.imagem') }}" alt="{{ Lang::get('frontend.base.lang.outraVersao') }}"> {{ Lang::get('frontend.base.lang.outraVersaoReduzido') }}
						</a>
					</li>
				</ul>
			</div>
		</div>
		<nav>
			<div class="centro">
				<a href="home" title="{{Lang::get('frontend.base.nav.paginaInicial')}}" id="link-home"><img src="assets/images/layout/logo.png" alt="{{Lang::get('frontend.base.nav.paginaInicial')}}"></a>
				<ul id="main-nav">
					<li>
						<a href="home" title="{{Lang::get('frontend.base.nav.home')}}" @if(str_is('home*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.home')}}</a>
					</li>
					<li class="comsub">
						<a href="empresa" id="mn-empresa" title="{{Lang::get('frontend.base.nav.empresa')}}" @if(str_is('quemsomos*', Route::currentRouteName()) || str_is('diferenciais*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.empresa')}} <div class="seta"></div></a>
						<ul class="submenu">
							<li><a href="empresa/quemsomos" title="{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}">{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}</a></li>
							<li><a href="empresa/diferenciais" title="{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}">{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}</a></li>
						</ul>
					</li>
					<li>
						<a href="solucoes" id="mn-solucoes" title="{{Lang::get('frontend.base.nav.solucoes')}}" @if(str_is('solucoes*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.solucoes')}}</a>
					</li>
					<li>
						<a href="https://www.vagas.com.br/metabpo" target="_blank" title="{{Lang::get('frontend.base.nav.oportunidades')}}">{{Lang::get('frontend.base.nav.oportunidades')}}</a>
					</li>
					<li class="comsub">
						<a href="cases" id="mn-cases" title="{{Lang::get('frontend.base.nav.cases')}}" @if(str_is('cases*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.cases')}} <div class="seta"></div></a>
						<ul class="submenu">
							<li><a href="cases/cases" title="{{Lang::get('frontend.base.nav.subcases.cases')}}">{{Lang::get('frontend.base.nav.subcases.cases')}}</a></li>
							<li><a href="cases/depoimentos" title="{{Lang::get('frontend.base.nav.subcases.depoimentos')}}">{{Lang::get('frontend.base.nav.subcases.depoimentos')}}</a></li>
						</ul>
					</li>
					<li>
						<a href="contato" title="{{Lang::get('frontend.base.nav.contato')}}" @if(str_is('contato*', Route::currentRouteName())) class='ativo' @endif>{{Lang::get('frontend.base.nav.contato')}}</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		@yield('conteudo')
	</div>

	<div class="barra-servicos">
		<div class="centro">
			<div class="conheca">
				<span class="linha linha1">{{Lang::get('frontend.base.barra.linha1')}}</span>
				<span class="linha linha2">{{Lang::get('frontend.base.barra.linha2')}}</span>
				<span class="linha linha3">{{Lang::get('frontend.base.barra.linha3')}}</span>
			</div>
			<div class="solucoes">
				@foreach($solucoes as $solucao)
				<a href="solucoes/{{$solucao->slug}}" class="{{$solucao->slug}}">
					<div class="icone"></div>
					<div class="sombra"></div>
					<span>{{ $solucao->{Lang::get('frontend.base.campos.titulo')} }}</span>
				</a>
				@endforeach
			</div>
		</div>
	</div>

	@if(str_is('home', Route::currentRouteName()))
		@include('frontend.partials.metanews')
	@endif

	<footer>
		<div class="centro">
			<div class="footer-coluna coluna1">
				<ul class="topmenu">
					<li>
						<a href="home" title="{{Lang::get('frontend.base.nav.home')}}">{{Lang::get('frontend.base.nav.home')}}</a>
					</li>
					<li class="comsub">
						<a href="empresa" title="{{Lang::get('frontend.base.nav.empresa')}}">{{Lang::get('frontend.base.nav.empresa')}}</a>
						<ul class="submenu">
							<li><a href="empresa/quemsomos" title="{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}">{{Lang::get('frontend.base.nav.subempresa.quemsomos')}}</a></li>
							<li><a href="empresa/diferenciais" title="{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}">{{Lang::get('frontend.base.nav.subempresa.diferenciais')}}</a></li>
						</ul>
					</li>
					<li class="comsub">
						<a href="solucoes" title="{{Lang::get('frontend.base.nav.solucoes')}}">{{Lang::get('frontend.base.nav.solucoes')}}</a>
						<ul class="submenu">
							@foreach($solucoes as $solucao)
							<li><a href="solucoes/{{$solucao->slug}}" title="{{ $solucao->{Lang::get('frontend.base.campos.titulo')} }}">{{ $solucao->{Lang::get('frontend.base.campos.titulo')} }}</a></li>
							@endforeach
						</ul>
					</li>
					<li>
						<a href="https://www.vagas.com.br/metabpo" target="_blank" title="{{Lang::get('frontend.base.nav.oportunidades')}}">{{Lang::get('frontend.base.nav.oportunidadesExtendido')}}</a>
					</li>
				</ul>
			</div>
			<div class="footer-coluna coluna2">
				<ul class="topmenu">
					<li class="comsub">
						<a href="cases" title="{{Lang::get('frontend.base.nav.cases')}}">{{Lang::get('frontend.base.nav.cases')}}</a>
						<ul class="submenu">
							<li><a href="cases/cases" title="{{Lang::get('frontend.base.nav.subcases.cases')}}">{{Lang::get('frontend.base.nav.subcases.cases')}}</a></li>
							<li><a href="cases/depoimentos" title="{{Lang::get('frontend.base.nav.subcases.depoimentos')}}">{{Lang::get('frontend.base.nav.subcases.depoimentos')}}</a></li>
						</ul>
					</li>
					<li>
						<a href="contato" title="{{Lang::get('frontend.base.nav.contato')}}">{{Lang::get('frontend.base.nav.contato')}}</a>
					</li>
				</ul>
				<div class="grupo">
					<p>{{Lang::get('frontend.base.footer.grupo')}}</p>
					<a href="http://www.grupometarh.com.br" target="_blank"><img src="assets/images/layout/footer-grupometarh.png" alt="Grupo Meta RH"></a>
				</div>
			</div>
			<div class="footer-coluna coluna3">
				<div class="social">
					@if($contato->facebook)
						<a href="{{ $contato->facebook }}" class="fb-link" target="_blank" title="Facebook">Facebook</a>
					@endif
					@if($contato->twitter)
						<a href="{{ $contato->twitter }}" class="tw-link" target="_blank" title="Twitter">Twitter</a>
					@endif
					@if($contato->linkedin)
						<a href="{{ $contato->linkedin }}" class="lk-link" target="_blank" title="Linkedin">Linkedin</a>
					@endif
				</div>
				@if($contato->telefone)
					<div class="telefone">{{ Tools::formataTelefone($contato->telefone) }}</div>
				@endif
				@if($contato->endereco)
					<div class="endereco">{{$contato->endereco}}</div>
				@endif
				<a href="politica-de-privacidade" class="privacy">{{ Lang::get('frontend.privacidade') }}</a>
				<div class="assinatura">
					&copy; {{date('Y')}} {{ Lang::get('frontend.base.footer.direitos') }}<br>
					<a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa">Criação de sites: Trupe Agência Criativa <img src="assets/images/layout/footer-trupe.png" alt="Trupe Agência Criativa"></a>
				</div>
			</div>
		</div>
	</footer>

	<?=Assets::JS(array(
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/jquery.cycle/jquery.cycle.all',
		'js/main'
	))?>

<!-- Código do Google para tag de remarketing -->


</body>
</html>
