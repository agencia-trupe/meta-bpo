@section('conteudo')

	<div id="slides-home">
		@if($banners)
			<div id="slides">
				@foreach($banners as $k => $banner)
					<div class="slide" @if($k > 0) style='display:none;' @endif>
						<a href="{{$banner->link}}" style="background-image:url('assets/images/banners/{{$banner->imagem}}');" data-texto="{{ $banner->{Lang::get('frontend.base.campos.texto')} }}">
							<img src="assets/images/banners/{{$banner->imagem}}">
						</a>
					</div>
				@endforeach
			</div>
				<a href="#" class="overlay" id="slides-overlay">
					<div class="centro">
						<div class="bg">
							<div class="arrow"></div>
							<div class="texto-wrapper"><span class="texto"></span></div>
						</div>
						<div id="slides-nav"></div>
					</div>
				</a>
			</div>
		@endif
	</div>

@stop
