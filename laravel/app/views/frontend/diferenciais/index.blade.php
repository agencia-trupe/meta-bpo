@section('conteudo')
	
	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			<span>{{ Lang::get('frontend.empresa.titulo') }} &raquo;</span> {{ Lang::get('frontend.empresa.diferenciais') }}
		</h1>
	</dix>

	<div class="conteudo">
		<div class="centro">
			<div class="texto cke">
				{{ $texto->{Lang::get('frontend.base.campos.texto')} }}
				<a href="empresa/quemsomos" class="link-quemsomos" title="{{Lang::get('frontend.empresa.quemsomos')}}">
					<span>{{Lang::get('frontend.empresa.quemsomos')}} &raquo;</span>
				</a>
			</div>

			<div class="imagem">
				<img src="assets/images/layout/diferenciais-foto.jpg" alt="{{ Lang::get('frontend.empresa.diferenciais') }}">
			</div>
		</div>
	</div>

@stop
