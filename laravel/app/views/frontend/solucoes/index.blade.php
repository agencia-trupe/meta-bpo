@section('conteudo')

	<dix id="faixa-azul-titulo">
		<h1 class="centro">
			{{ Str::upper(Lang::get('frontend.base.nav.solucoes')) }}
		</h1>
	</dix>

	<div class="conteudo">
		<div class="centro">
			<aside>
				<div class="selecionado {{ $selecionado->slug }}"></div>
				@foreach($solucoes as $solucao)
					<a href="solucoes/{{ $solucao->slug }}" class="solucao {{ $solucao->slug }}">
						<div class="icone"></div>
						<span>{{ $solucao->{Lang::get('frontend.base.campos.titulo')} }}</span>
					</a>
				@endforeach
			</aside>
			<div class="aside-bg"></div>
			<div class="linhas"></div>

			<section class="{{ $selecionado->slug }}">
				<h2>{{ $selecionado->{Lang::get('frontend.base.campos.titulo')} }}</h2>
				<div class="texto">
					{{ $selecionado->{Lang::get('frontend.base.campos.texto')} }}
				</div>
			</section>
		</div>
	</div>

@stop
