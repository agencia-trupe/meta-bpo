<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, Tools, File, Oportunidade;

class OportunidadesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	public function __construct(){
		Oportunidade::atualizarExpiradas();		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$idioma = Input::get('idioma');
		
		if(!Input::has('idioma') || ($idioma != 'pt' && $idioma != 'en'))
			$idioma = 'pt';

		$this->layout->content = View::make('backend.oportunidades.index')->with('registros', Oportunidade::idioma($idioma)->ordenado()->paginate(25))
																		  ->with('idioma', $idioma);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.oportunidades.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Oportunidade;

		$object->empresa = Input::get('empresa');
		$object->titulo = Input::get('titulo');
		$object->n_posicoes = Input::get('n_posicoes');
		$object->cidade = Input::get('cidade');
		$object->estado = Input::get('estado');
		$object->data_cadastro = Tools::converteData(Input::get('data_cadastro'));
		
		if(Input::get('data_expiracao') != '')
			$object->data_expiracao = Tools::converteData(Input::get('data_expiracao'));

		$object->texto = Input::get('texto');
		$object->idioma = Input::get('idioma');
		
		try {

			$object->save();

			$object->slug = Str::slug($object->id.'-'.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Oportunidade criada com sucesso.');
			return Redirect::route('painel.oportunidades.index', array('idioma' => $object->idioma));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Oportunidade!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.oportunidades.edit')->with('registro', Oportunidade::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Oportunidade::find($id);

		$object->empresa = Input::get('empresa');
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug($object->id.'-'.$object->titulo);
		$object->n_posicoes = Input::get('n_posicoes');
		$object->cidade = Input::get('cidade');
		$object->estado = Input::get('estado');
		$object->data_cadastro = Tools::converteData(Input::get('data_cadastro'));

		if(Input::get('data_expiracao') != '')
			$object->data_expiracao = Tools::converteData(Input::get('data_expiracao'));

		$object->texto = Input::get('texto');
		$object->idioma = Input::get('idioma');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Oportunidade alterada com sucesso.');
			return Redirect::route('painel.oportunidades.index', array('idioma' => $object->idioma));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Oportunidade!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Oportunidade::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Oportunidade arquivada com sucesso.');

		return Redirect::route('painel.oportunidades.index', array('idioma' => $object->idioma));
	}

}