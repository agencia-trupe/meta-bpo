<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Depoimento;

class DepoimentosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.depoimentos.index')->with('registros', Depoimento::ordenado());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.depoimentos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Depoimento;

		$object->empresa = Input::get('empresa');
		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		$object->autoria_pt = Input::get('autoria_pt');
		$object->autoria_en = Input::get('autoria_en');
		
		$imagem = Thumb::make('imagem', 290, null, 'depoimentos/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Depoimento criado com sucesso.');
			return Redirect::route('painel.depoimentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Depoimento!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.depoimentos.edit')->with('registro', Depoimento::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Depoimento::find($id);

		$object->empresa = Input::get('empresa');
		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');
		$object->autoria_pt = Input::get('autoria_pt');
		$object->autoria_en = Input::get('autoria_en');

		$imagem = Thumb::make('imagem', 290, null, 'depoimentos/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Depoimento alterado com sucesso.');
			return Redirect::route('painel.depoimentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Depoimento!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Depoimento::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Depoimento removido com sucesso.');

		return Redirect::route('painel.depoimentos.index');
	}

}