<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Cases;

class CasesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cases.index')->with('registros', Cases::ordenado());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cases.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Cases;

		$object->empresa = Input::get('empresa');
		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');		

		$t = date('ymdHis');
		$imagem = Thumb::make('imagem', 380, 220, 'cases/', $t, false);
		Thumb::make('imagem', 180, 110, 'cases/thumbs/', $t, false);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Case criado com sucesso.');
			return Redirect::route('painel.cases.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Case!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cases.edit')->with('registro', Cases::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Cases::find($id);

		$object->empresa = Input::get('empresa');
		$object->titulo_pt = Input::get('titulo_pt');
		$object->titulo_en = Input::get('titulo_en');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');		

		$t = date('ymdHis');
		$imagem = Thumb::make('imagem', 380, 220, 'cases/', $t, false);
		Thumb::make('imagem', 180, 110, 'cases/thumbs/', $t, false);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Case alterado com sucesso.');
			return Redirect::route('painel.cases.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Case!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Cases::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Case removido com sucesso.');

		return Redirect::route('painel.cases.index');
	}

}