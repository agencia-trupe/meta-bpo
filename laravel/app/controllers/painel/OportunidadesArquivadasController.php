<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, Tools, File, Oportunidade;

class OportunidadesArquivadasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		Oportunidade::atualizarExpiradas();

		$idioma = Input::get('idioma');
		
		if(!Input::has('idioma') || ($idioma != 'pt' && $idioma != 'en'))
			$idioma = 'pt';

		$this->layout->content = View::make('backend.oportunidadesarquivadas.index')->with('registros', Oportunidade::onlyTrashed()->idioma($idioma)->ordenado()->paginate(25))
																		  ->with('idioma', $idioma);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.oportunidadesarquivadas.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.oportunidadesarquivadas.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.oportunidadesarquivadas.edit')->with('registro', Oportunidade::withTrashed()->findOrFail($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$reativar = (Input::get('submitbtn') == 'reativar') ? true : false;
		
		$object = Oportunidade::withTrashed()->find($id);

		$object->empresa = Input::get('empresa');
		$object->titulo = Input::get('titulo');
		$object->n_posicoes = Input::get('n_posicoes');
		$object->cidade = Input::get('cidade');
		$object->estado = Input::get('estado');
		$object->data_cadastro = Tools::converteData(Input::get('data_cadastro'));

		if(Input::get('data_expiracao') != '')
			$object->data_expiracao = Tools::converteData(Input::get('data_expiracao'));

		$object->texto = Input::get('texto');
		$object->idioma = Input::get('idioma');


		try {

			$object->save();

			if($reativar){
				$object->restore();
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Oportunidade alterada e Reativada com sucesso.');
				return Redirect::route('painel.oportunidades.index', array('idioma' => $object->idioma));
			}else{
				Session::flash('sucesso', true);
				Session::flash('mensagem', 'Oportunidade alterada com sucesso.');
				return Redirect::route('painel.oportunidadesarquivadas.index', array('idioma' => $object->idioma));
			}

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Oportunidade!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::route('painel.oportunidadesarquivadas.index');
	}

}