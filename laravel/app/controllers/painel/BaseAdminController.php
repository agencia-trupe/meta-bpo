<?php

namespace Painel;

use \View;

class BaseAdminController extends \Controller {

	protected $layout = 'backend.templates.index';

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
