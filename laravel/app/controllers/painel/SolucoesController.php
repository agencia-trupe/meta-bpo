<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Solucao;

class SolucoesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '3';

	public function index()
	{
		$this->layout->content = View::make('backend.solucoes.index')->with('registros', Solucao::all());
	}

	public function create()
	{
		return Redirect::back();
	}

	public function store()
	{
		return Redirect::back();
	}

	public function show($id)
	{
		return Redirect::back();
	}

	public function edit($id)
	{
		$this->layout->content = View::make('backend.solucoes.edit')->with('registro', Solucao::find($id));
	}

	public function update($id)
	{
		$object = Solucao::find($id);

		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.solucoes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	public function destroy($id)
	{
		return Redirect::back();
	}

}
