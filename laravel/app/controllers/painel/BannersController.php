<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Banner;

class BannersController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.banners.index')->with('registros', Banner::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.banners.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Banner;

		$t = Date('YmdHis');
		$imagem = Thumb::make('imagem', null, null, 'banners/', $t);
		Thumb::make('imagem', 80, 80, 'banners/thumbs/', $t, true);
		if($imagem) $object->imagem = $imagem;

		$object->link     = Input::get('link');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner criado com sucesso.');
			return Redirect::route('painel.banners.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.banners.edit')->with('registro', Banner::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Banner::find($id);

		$t = Date('YmdHis');
		$imagem = Thumb::make('imagem', null, null, 'banners/', $t);
		Thumb::make('imagem', 80, 80, 'banners/thumbs/', $t, true);
		if($imagem) $object->imagem = $imagem;

		$object->link     = Input::get('link');
		$object->texto_pt = Input::get('texto_pt');
		$object->texto_en = Input::get('texto_en');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner alterado com sucesso.');
			return Redirect::route('painel.banners.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Banner::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Banner removido com sucesso.');

		return Redirect::route('painel.banners.index');
	}

}
