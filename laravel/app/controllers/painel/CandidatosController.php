<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, Tools, File, Candidato;

class CandidatosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.candidatos.index')->with('registros', Candidato::paginate(25));		
	}

	public function buscarNome()
	{
		$nome = Input::get('nome');

		$this->layout->content = View::make('backend.candidatos.busca.nome')->with('registros', Candidato::where('nome', 'like', '%'.$nome.'%')->get())
																		    ->with('termo', $nome);
	}

	public function buscarData()
	{
		$inicial = Input::get('dt_inicial');
		$final = Input::get('dt_final');

		if($final == ''){
			$ops = Candidato::where(\DB::raw("DATE(created_at)"), '=', Tools::converteData($inicial))->get();
		}else{
			$ops = Candidato::where(\DB::raw("DATE(created_at)"), '>=', Tools::converteData($inicial))
								->where(\DB::raw("DATE(created_at)"), '<=', Tools::converteData($final))
								->get();
		}
		
		$this->layout->content = View::make('backend.candidatos.busca.data')->with('registros', $ops)
																		    ->with('termo_inicial', $inicial)
																		    ->with('termo_final', $final);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.candidatos.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.candidatos.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Redirect::route('painel.candidatos.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Redirect::route('painel.candidatos.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return Redirect::route('painel.candidatos.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Candidato::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Candidato removido com sucesso.');

		return Redirect::route('painel.candidatos.index');
	}

}