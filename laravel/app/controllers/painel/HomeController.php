<?php

namespace Painel;

use \View;

class HomeController extends BaseAdminController {

    public function index()
	{
		$this->layout->content = View::make('backend.home');
	}

	public function login()
	{
		$this->layout->content = View::make('backend.login');
	}

}
