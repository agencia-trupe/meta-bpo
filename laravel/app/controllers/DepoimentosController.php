<?php

use \Depoimento;

class DepoimentosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.depoimentos.index')->with('depoimentos', Depoimento::ordenado());
	}

}
