<?php

use \Cases;

class CasesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.cases.index')->with('cases', Cases::ordenado());
	}

}
