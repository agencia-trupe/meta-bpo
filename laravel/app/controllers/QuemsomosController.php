<?php

use \QuemSomos, \Equipe;

class QuemsomosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.quemsomos.index')->with('texto', QuemSomos::first());
	}

}
