<?php

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = \View::make('frontend.contato.index')->with('contato', \Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = \Input::get('nome');
		$data['email'] = \Input::get('email');
		$data['telefone'] = \Input::get('telefone');
		$data['mensagem'] = \Input::get('mensagem');
		$data['ip_envio'] = Tools::ip();

		if($data['nome'] && $data['email'] && $data['mensagem']){
			\Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('metabpo@metabpo.com.br', 'Meta BPO')
			    		->subject('Contato via site')
			    		->replyTo($data['email'], $data['nome']);
			});
		}

		\Session::flash('enviado', true);
		return \Redirect::route('contato');
	}
}
