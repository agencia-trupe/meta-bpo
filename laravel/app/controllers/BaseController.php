<?php

use \Contato, \Solucao;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$contato  = Contato::first();
			$solucoes = Solucao::all();
			$this->layout = View::make($this->layout)->with(compact('contato', 'solucoes'));
		}
	}

}
