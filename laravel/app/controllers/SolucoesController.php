<?php

use \Solucao;

class SolucoesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($slug = null)
	{
        if (!$slug) {
        $selecionado = Solucao::first();
        } else {
            $selecionado = Solucao::where('slug', $slug)->first();
            if (!$selecionado) App::abort('404');
        }

        $solucoes = Solucao::where('slug', '!=', $selecionado->slug)->get();

		$this->layout->content = View::make('frontend.solucoes.index')
            ->with(compact('selecionado', 'solucoes'));
	}

}
