<?php

use \Oportunidade;

class OportunidadesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function __construct(){
		Oportunidade::atualizarExpiradas();		
	}

	public function index()
	{
		$this->layout->content = View::make('frontend.oportunidades.index')->with('oportunidades', Oportunidade::ordenado()->paginate(10));
	}

	public function detalhe($slug = '')
	{
		if(!$slug) App::abort('404');
		
		$detalhe = Oportunidade::where('slug', '=', $slug)->first();
		
		if(is_null($detalhe)) App::abort('404');

		$this->layout->content = View::make('frontend.oportunidades.detalhes')->with('detalhes', $detalhe);	
	}

	public function enviar()
	{
		$error = false;
		$nome = Input::get('nome');
		$email = Input::get('email');
		$telefone = Input::get('telefone');
		$oportunidades_id = Input::get('oportunidades_id');
		$observacoes = Input::get('observacoes');

		if(!$nome)
			$error = "Informe seu Nome!";

		if(!$email && $error === false)
			$error = "Informe seu Email!";

		if(!$oportunidades_id && $error === false)
			$error = "Oportunidade não encontrada!";

		if(!Input::hasFile('curriculo') && $error === false)
			$error = "Selecione um arquivo!";		

		$curriculo = Input::file('curriculo');
		$rules = array(
            'file' => 'required|mimes:pdf,doc,docx'
        );
        $validator = \Validator::make(array('file'=> $curriculo), $rules);

        if(!$validator->passes())
        	$error = "O Currículo deve estar no formato PDF ou DOC/DOCX";

		if($error === false){

			$curriculo = Input::file('curriculo');
			$arquivo = date('dmYHis').$curriculo->getClientOriginalName();

			// Armazenar CV em base_path('curriculos/arquivo')
			$curriculo->move(base_path('curriculos'), $arquivo);

			$cand = new \Candidato;
			$cand->oportunidades_id = $oportunidades_id;
			$cand->nome = $nome;
			$cand->email = $email;
			$cand->telefone = $telefone;
			$cand->observacoes = $observacoes;
			$cand->curriculo = $arquivo;

			$cand->save();

			$data['nome'] = $nome;
			$data['email'] = $email;
			$data['telefone'] = $telefone;
			$data['observacoes'] = $observacoes;
			$data['arquivo'] = $arquivo;
			$data['vaga'] = \Oportunidade::find($oportunidades_id)->empresa.' - '.\Oportunidade::find($oportunidades_id)->titulo;

			if($data['nome'] && $data['email'] && $data['observacoes']){
				Mail::send('emails.candidato', $data, function($message) use ($data)
				{
				    $message->to('contato@metaexecutivos.com.br', 'Meta Executivos')
				    		->subject('Candidato para vaga '.$data['vaga'])
				    		->attach(base_path('curriculos/'.$data['arquivo']))
				    		->replyTo($data['email'], $data['nome']);
				});
			}
		}else{
			Session::flash('form-nome', $nome);
			Session::flash('form-email', $email);
			Session::flash('form-telefone', $telefone);
			Session::flash('form-observacoes', $observacoes);
			return Redirect::back()->withErrors(array($error));
		}

		Session::flash('enviado', true);
		return Redirect::back();
	}
}
