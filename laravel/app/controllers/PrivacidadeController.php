<?php

use \Privacidade, \Equipe;

class PrivacidadeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.privacidade.index')->with('texto', Privacidade::first());
	}

}
