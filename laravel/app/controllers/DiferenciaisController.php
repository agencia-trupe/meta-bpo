<?php

use \Diferenciais;

class DiferenciaisController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.diferenciais.index')->with('texto', Diferenciais::first());
	}

}
