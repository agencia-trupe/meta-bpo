<?php

use \Banner;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$urlJson = 'http://www.metanews.com.br/mais-recentes';
		$str = file_get_contents($urlJson);
		$metanews = json_decode($str, true);
		View::share(compact('metanews'));

		$banners = Banner::orderBy('ordem', 'asc')->get();

		$this->layout->content = View::make('frontend.home.index')->with(compact('banners'));
	}

}
