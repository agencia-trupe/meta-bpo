<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'telefone' => '+55 11 5525-2721',
                'facebook' => 'https://www.facebook.com/metabpo1',
                'twitter' => 'https://twitter.com/MetaBPO_',
                'linkedin' => 'https://www.linkedin.com/company/meta-bpo',
                'google_maps' => '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?q=Av.+Adolfo+Pinheiro,+1001&amp;ie=UTF8&amp;hq=&amp;hnear=Avenida+Adolfo+Pinheiro,+1001+-+Santo+Amaro,+S%C3%A3o+Paulo,+04733-200&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.644777,-46.700475&amp;output=embed"></iframe>',
				'endereco' => '<p>Av. Adolfo Pinheiro, 1001 &bull; 15&ordm; andar<br>Alto da Boa Vista<br>04733-100 &bull; São Paulo/SP</p>'
            )
        );

        DB::table('novosite_contato')->insert($data);
    }

}
