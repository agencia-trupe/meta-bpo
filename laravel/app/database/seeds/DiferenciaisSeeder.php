<?php

class DiferenciaisSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'texto_pt' => '<p>Texto em Português</p>',
				'texto_en' => '<p>Texto em Inglês</p>'
            )
        );

        DB::table('novosite_diferenciais')->insert($data);
    }

}
