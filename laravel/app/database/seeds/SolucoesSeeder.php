<?php

class SolucoesSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
                'slug'      => 'recrutamento-e-selecao',
                'titulo_pt' => 'Recrutamento & Seleção',
				'titulo_en' => 'Recruitment & Selection',
				'texto_pt'  => '<p>Texto em Português</p>',
                'texto_en'  => '<p>Texto em Inglês</p>'
            ),
            array(
                'slug'      => 'temporarios',
                'titulo_pt' => 'Temporários',
                'titulo_en' => 'Temporary',
                'texto_pt'  => '<p>Texto em Português</p>',
                'texto_en'  => '<p>Texto em Inglês</p>'
            ),
            array(
                'slug'      => 'outsourcing-bpo',
                'titulo_pt' => 'Outsourcing | BPO',
                'titulo_en' => 'Outsourcing | BPO',
                'texto_pt'  => '<p>Texto em Português</p>',
                'texto_en'  => '<p>Texto em Inglês</p>'
            )
        );

        DB::table('novosite_solucoes')->insert($data);
    }

}
