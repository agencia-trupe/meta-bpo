<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersSeeder');
		$this->call('QuemSomosSeeder');
		$this->call('DiferenciaisSeeder');
		$this->call('SolucoesSeeder');
		$this->call('ContatoSeeder');
	}

}
