<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novosite_cases', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('empresa');
			$table->string('imagem');
			$table->string('titulo_pt');
			$table->string('titulo_en');
			$table->text('texto_pt');
			$table->text('texto_en');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novosite_cases');
	}

}
