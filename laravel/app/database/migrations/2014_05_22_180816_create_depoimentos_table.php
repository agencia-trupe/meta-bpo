<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepoimentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novosite_depoimentos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('empresa');
			$table->string('titulo_pt');
			$table->string('titulo_en');
			$table->text('texto_pt');
			$table->text('texto_en');
			$table->string('autoria_pt');
			$table->string('autoria_en');
			$table->string('imagem');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novosite_depoimentos');
	}

}
