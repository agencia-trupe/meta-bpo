<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novosite_contato', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('telefone');
			$table->string('facebook');
			$table->string('twitter');
			$table->string('linkedin');
			$table->text('endereco');
			$table->text('google_maps');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novosite_contato');
	}

}
