<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novosite_banners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('link');
			$table->string('imagem');
			$table->text('texto_pt');
			$table->text('texto_en');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novosite_banners');
	}

}
