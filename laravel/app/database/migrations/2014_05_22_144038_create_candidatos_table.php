<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novosite_candidatos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('oportunidades_id');
			$table->string('nome');
			$table->string('email');
			$table->string('telefone');
			$table->text('observacoes');
			$table->string('curriculo');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novosite_candidatos');
	}

}
