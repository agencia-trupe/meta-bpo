<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOportunidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novosite_oportunidades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('empresa');
			$table->string('titulo');
			$table->string('slug');
			$table->string('n_posicoes');
			$table->string('cidade');
			$table->string('estado');
			$table->date('data_cadastro');
			$table->date('data_expiracao')->nullable();
			$table->text('texto');
			$table->string('idioma');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novosite_oportunidades');
	}

}
