<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolucoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novosite_solucoes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug');
			$table->string('titulo_pt');
			$table->string('titulo_en');
			$table->text('texto_pt');
			$table->text('texto_en');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novosite_solucoes');
	}

}
