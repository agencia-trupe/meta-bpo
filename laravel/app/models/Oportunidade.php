<?php

class Oportunidade extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'novosite_oportunidades';

	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('deleted_at', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public static function atualizarExpiradas(){
    	Oportunidade::chunk(50, function($ops){
    		foreach ($ops as $key => $op) {
	    		if($op->data_expiracao != '' && $op->data_expiracao != '0000-00-00' && $op->data_expiracao <= date('Y-m-d'))
	    			$op->delete();    			
    		}
    	});
    }

    public function scopeIdioma($query, $idioma){
    	return $query->where('idioma', $idioma);
    }

    public function scopeOrdenado($query){
    	return $query->orderBy('data_cadastro', 'DESC')->orderBy('id', 'DESC');
    }

    public function candidatos(){
    	return $this->hasMany('Candidatos', 'oportunidades_id');
    }
}