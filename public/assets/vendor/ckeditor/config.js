/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.extraPlugins = 'font,colorbutton';

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [];

	config.toolbar = [
	    [ 'Bold', 'Italic', 'Underline' ],
	    [ 'BulletedList' ],
	    [ 'Link', 'Unlink' ],
	    [ 'FontSize', 'TextColor' ]
	];

	config.fontSize_sizes = '12/12px;13/13px;14/14px;15/15px;16/16px;18/18px;20/20px;22/22px;24/24px;';
	config.colorButton_colors = '#000/000,#FFF/FFF,#62BBB1/62BBB1,#A6C28B/A6C28B,#BFAF40/BFAF40,#EADC75/EADC75';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};
