$('document').ready( function(){

	if($('#slides-home #slides .slide').length){
		$('#slides-home #slides').cycle({
			activePagerClass: 'activeSlide',
			pagerAnchorBuilder : function(index, DOMelement){
				return '<a href="#"></a>';
			},
			pause : true,
			pager : $('#slides-nav'),
			before: function (curr, next, opts) {
			    var $slideAtual = $(next).find('a'),
			        link  = $slideAtual.attr('href'),
			        texto = $slideAtual.data('texto');
			    $('#slides-overlay').attr('href', link);
			    $('#slides-overlay .texto-wrapper').fadeOut('fast', function() {
				    $('#slides-overlay .texto').html(texto);
				    $('#slides-overlay .texto-wrapper').fadeIn();
			    });
			}
		});
	}

	$('#mn-empresa, #mn-cases').click( function(e){e.preventDefault()});

	$('.cursos .curso .titulo').click( function(){
		$(this).toggleClass('aberto');
	});

	$('#input-curriculo').change( function(e){
		var arquivo = e.target.files[0].name;
		$('#form-candidato label span').html(arquivo).addClass('negrito');
	});

	$('.listaCases a').click( function(e){
		e.preventDefault();
		$('.listaCases a.aberto').removeClass('aberto');
		$('.detalhes').removeClass('aberto');
		$(this).addClass('aberto');
		var indice = $(this).attr('data-indice');
		var imagem = $(this).attr('data-imagem');
		var titulo = $(this).attr('data-titulo');
		var texto = $(this).attr('data-texto');
		var target = $(this).parent().next('div.detalhes');
		var miolo = target.find('.centro');
		var seta = miolo.find('.setaUp');

		var larguraTela = $(window).width();
		var larguraMiolo = parseInt($('header .centro').css('width'));
		var larguraEspacoLateral = (larguraTela - larguraMiolo) / 2;
		
		var offset = $(this).position();
		var deslocamento = offset.left - larguraEspacoLateral + 82;

		seta.css('left', deslocamento+'px');
		miolo.removeClass('aberto1 aberto2 aberto3 aberto4 aberto5').addClass('aberto'+indice);
		target.addClass('aberto')
		
		miolo.find('.imagem').html("<img src='assets/images/cases/"+imagem+"'>");
		miolo.find('.texto h2').html(titulo);
		miolo.find('.texto .cke').html(texto);		
	});

	$('.listaCases .detalhes .fechar').click( function(){
		$(this).parent().parent().removeClass('aberto');
	});

	$('.solucao').hover(
		function() { $('.selecionado').addClass('remove-line'); },
		function() { $('.selecionado').removeClass('remove-line'); }
	);
});
